from amaranth import *
from amaranth.lib.cdc import FFSynchronizer

from .dac_if import DACInterface
from .shiftreg_if import ShiftRegInterface
from .i2c_if import I2CInterface
from .spi_if_ljpll import SPIInterfaceLjpll
from .spi_if_lcdco import SPIInterfaceLcDco
from .hit_sampler import HitSampler

class MicrobeamSubtarget(Elaboratable):
    def __init__(self, dut, beam_if, regs, pads, leds, in_fifo, out_fifo):
        self.dut = dut  # one of "adpll" or "ljpll"
        self.beam_if = beam_if  # one of "write" (DAC) or "read" (parallel bus)
        self.in_fifo  = in_fifo
        self.out_fifo = out_fifo
        self.regs = regs
        self.pads = pads
        self.leds = leds

    def elaborate(self, platform):
        m = Module()

        # DUT interfacing
        m.d.comb += [
            self.pads.asic_reset_t.oe.eq(1),
        ]

        if self.dut == "adpll":
            m.submodules.i2c_if = i2c_if = I2CInterface(
                pads=self.pads,
                addr_len=1
            )

            m.d.comb += [
                self.regs["i2c_ack"].eq(i2c_if.ack),
                i2c_if.launch.eq(self.regs["i2c_launch"]),
                i2c_if.slave_addr.eq(self.regs["i2c_slave_addr"]),
                i2c_if.reg_addr.eq(self.regs["i2c_reg_addr"]),
                i2c_if.reg_data.eq(self.regs["i2c_reg_data"]),
                self.pads.asic_reset_t.o.eq(self.regs["asic_reset"]),
            ]
        elif self.dut == "ljpll":
            m.submodules.spi_if = spi_if = SPIInterfaceLjpll(
                pads=self.pads
            )

            m.d.comb += [
                spi_if.data.eq(self.regs["spi_data"]),
                spi_if.launch.eq(self.regs["spi_launch"]),
                # ljPLL uses inverted reset polarity
                self.pads.asic_reset_t.o.eq(~self.regs["asic_reset"]),
            ]
        elif self.dut == "dart28":
            m.submodules.i2c_if = i2c_if = I2CInterface(
                pads=self.pads,
                addr_len=2
            )

            m.d.comb += [
                self.regs["i2c_ack"].eq(i2c_if.ack),
                i2c_if.launch.eq(self.regs["i2c_launch"]),
                i2c_if.slave_addr.eq(self.regs["i2c_slave_addr"]),
                i2c_if.reg_addr.eq(self.regs["i2c_reg_addr"]),
                i2c_if.reg_data.eq(self.regs["i2c_reg_data"]),
                # DART28 test board uses inverted reset polarity
                self.pads.asic_reset_t.o.eq(~self.regs["asic_reset"]),
            ]
        elif self.dut == "lcdco":
            m.submodules.spi_if = spi_if = SPIInterfaceLcDco(
                pads=self.pads
            )

            m.d.comb += [
                spi_if.data.eq(self.regs["spi_data"]),
                spi_if.launch.eq(self.regs["spi_launch"]),
                # LC DCO chip uses inverted reset polarity
                self.pads.asic_reset_t.o.eq(~self.regs["asic_reset"]),
            ]
        else:
            raise ValueError("Invalid DUT type specified!")

        ## Facility interfacing
        # Trigger + trigger edge detection
        trigger_resamp = Signal()
        trigger_resamp_d = Signal()
        trigger_posedge = Signal()
        m.d.comb += [
            self.pads.mcb_trigger_t.oe.eq(0),
        ]
        m.submodules.trigger_samp = FFSynchronizer(self.pads.mcb_trigger_t.i, trigger_resamp)

        m.d.sync += trigger_resamp_d.eq(trigger_resamp)
        m.d.comb += trigger_posedge.eq(trigger_resamp & (~trigger_resamp_d))

        hit_sampler_pos_ready = Signal()
        pos_x = Signal(16)  # beam position
        pos_y = Signal(16)  # beam position

        if self.beam_if == "write":  # add DAC interfce
            m.submodules.dac_if = dac_if = DACInterface(
                fifo=self.out_fifo,
                sclk=self.pads.dac_sclk_t,
                sdin=self.pads.dac_sdin_t,
                sync=self.pads.dac_sync_t,
                ldac=self.pads.dac_ldac_t,
            )
            m.d.comb += [
                hit_sampler_pos_ready.eq(1),
                pos_x.eq(dac_if.pos_x),
                pos_y.eq(dac_if.pos_y),
            ]
        elif self.beam_if == "read":  # add SR interface
            m.submodules.sr = sr_if = ShiftRegInterface(
                sclk=self.pads.sr_clk_t,
                dout=self.pads.sr_dout_t,
                shld=self.pads.sr_shld_t,
            )
            m.d.comb += [
                hit_sampler_pos_ready.eq(sr_if.pos_ready),
                sr_if.trigger.eq(trigger_posedge),
                pos_x.eq(Cat(sr_if.pos_x, C(0, 4))),
                pos_y.eq(Cat(sr_if.pos_y, C(0, 4))),
            ]
        else:
            raise ValueError("Invalid beam interface type specified!")

        shutter_pre_override = Signal()

        m.submodules.hitsampler = hs = HitSampler(
            fifo=self.in_fifo,
            trigger=trigger_posedge,
            shutter=shutter_pre_override,
        )


        m.d.comb += [
            hs.pos_x.eq(pos_x),
            hs.pos_y.eq(pos_y),
            hs.pos_ready.eq(hit_sampler_pos_ready),
            hs.shutter_time.eq(self.regs["shutter_time"]),
            self.pads.mcb_shutter_t.oe.eq(1),
            self.pads.mcb_shutter_t.o.eq(shutter_pre_override | self.regs["shutter_override"]),
        ]

        prescaler = Signal(24)  # temp
        m.d.sync += prescaler.eq(prescaler + 1)
        #m.d.comb += self.pads.mcb_shutter_t.o.eq(prescaler[-1])

        return m
