from amaranth import *

class SPIInterfaceLjpll(Elaboratable):
    def __init__(self, pads):
        self.pads = pads
        self.launch = Signal(1)
        self.data = Signal(92)

    def elaborate(self, platform):
        m = Module()

        # set all pads to output
        m.d.comb += [
            self.pads.scl_t.oe.eq(1),
            self.pads.sda_t.oe.eq(1),
            self.pads.asic_scapt_t.oe.eq(1),
        ]
        
        # SPI state machine clock source
        DIV_CYC = 128
        tick = Signal(1)
        div = Signal(range(DIV_CYC+1))
        with m.If(div != 0):
            m.d.sync += div.eq(div - 1)
            m.d.sync += tick.eq(0)
        with m.Else():
            m.d.sync += div.eq(DIV_CYC)
            m.d.sync += tick.eq(1)


        with m.FSM():
            spi_data = Signal(92)
            count = Signal(range(92))
            with m.State("IDLE"):
                m.d.sync += [
                    self.pads.scl_t.o.eq(0),
                    self.pads.sda_t.o.eq(0),
                    self.pads.asic_scapt_t.o.eq(0),
                    count.eq(92)
                ]
                with m.If(self.launch):
                    m.d.sync += spi_data.eq(self.data)
                    m.next = "PUT_DATA"
            with m.State("PUT_DATA"):
                with m.If(tick):
                    m.d.sync += self.pads.scl_t.o.eq(0)
                    m.d.sync += self.pads.sda_t.o.eq(spi_data[91])
                    m.d.sync += spi_data.eq(Cat(C(0, 1), spi_data))
                    with m.If(count == 0):
                        m.next = "PUT_SCAPT"
                    with m.Else():
                        m.next = "PUT_CLOCK"
            with m.State("PUT_CLOCK"):
                with m.If(tick):
                    m.d.sync += self.pads.scl_t.o.eq(1)
                    m.d.sync += count.eq(count - 1)
                    m.next = "PUT_DATA"
            with m.State("PUT_SCAPT"):
                with m.If(tick):
                    m.d.sync += self.pads.asic_scapt_t.o.eq(1)
                    m.next = "TAKE_SCAPT"
            with m.State("TAKE_SCAPT"):
                with m.If(tick):
                    m.d.sync += self.pads.asic_scapt_t.o.eq(0)
                    m.next = "WAIT"
            with m.State("WAIT"):
                with m.If(tick & ~self.launch):
                    m.next = "IDLE"


        return m
