from amaranth import *

class DACInterface(Elaboratable):
    def __init__(self, fifo, sclk, sdin, sync, ldac):
        self.fifo = fifo
        self.sclk = sclk
        self.sdin = sdin
        self.sync = sync
        self.ldac = ldac
        self.pos_x = Signal(16)
        self.pos_y = Signal(16)

    def elaborate(self, platform):
        m = Module()

        m.d.comb += [
            self.sclk.oe.eq(1),
            self.sync.oe.eq(1),
            self.ldac.oe.eq(1),
            self.sdin.oe.eq(1),
        ]
        
        # SPI state machine clock source
        DIV_CYC = 16
        tick = Signal(1)
        div = Signal(range(DIV_CYC))
        with m.If(div != 0):
            m.d.sync += div.eq(div - 1)
            m.d.sync += tick.eq(0)
        with m.Else():
            m.d.sync += div.eq(DIV_CYC-1)
            m.d.sync += tick.eq(1)

        # FIFO interface
        spi_done = Signal(1)
        spi_start = Signal(1)
        spi_data = Signal(24)

        with m.FSM():
            val_xl = Signal(8)
            val_yl = Signal(8)
            val_xh = Signal(8)
            val_yh = Signal(8)
            m.d.sync += spi_start.eq(0)

            with m.State("IDLE_XL"):
                with m.If(self.fifo.r_rdy):
                    m.d.comb += self.fifo.r_en.eq(1)
                    m.d.sync += val_xl.eq(self.fifo.r_data),
                    m.next = "WAIT_XH"
            with m.State("WAIT_XH"):
                with m.If(self.fifo.r_rdy):
                    m.d.comb += self.fifo.r_en.eq(1)
                    m.d.sync += val_xh.eq(self.fifo.r_data),
                    m.next = "WAIT_YL"
            with m.State("WAIT_YL"):
                with m.If(self.fifo.r_rdy):
                    m.d.comb += self.fifo.r_en.eq(1)
                    m.d.sync += val_yl.eq(self.fifo.r_data),
                    m.next = "WAIT_YH"
            with m.State("WAIT_YH"):
                with m.If(self.fifo.r_rdy):
                    m.d.comb += self.fifo.r_en.eq(1)
                    m.d.sync += val_yh.eq(self.fifo.r_data),
                    m.next = "LAUNCH_TX_X"
            with m.State("LAUNCH_TX_X"):
                    m.d.sync += spi_data.eq(Cat(val_xl, val_xh, Const(0b00010000, unsigned(8))))
                    m.d.sync += spi_start.eq(1)
                    m.d.sync += self.ldac.o.eq(1)
                    m.next = "WAIT_TX_X"
            with m.State("WAIT_TX_X"):
                with m.If(spi_done):
                    m.next = "LAUNCH_TX_Y"
            with m.State("LAUNCH_TX_Y"):
                    m.d.sync += spi_data.eq(Cat(val_yl, val_yh, Const(0b00010001, unsigned(8))))
                    m.d.sync += spi_start.eq(1)
                    m.next = "WAIT_TX_Y"
            with m.State("WAIT_TX_Y"):
                with m.If(spi_done):
                    # Latch X+Y positions in DAC & update hit sampler input
                    m.d.sync += self.ldac.o.eq(0)
                    m.d.sync += self.pos_x.eq(Cat(val_xl, val_xh))
                    m.d.sync += self.pos_y.eq(Cat(val_yl, val_yh))
                    m.next = "WAIT_LDAC"
            with m.State("WAIT_LDAC"):
                with m.If(tick):
                    m.next = "IDLE_XL"

        # SPI business logic
        with m.FSM():
            sr = Signal(24)
            count = Signal(range(24))

            with m.State("IDLE"):
                m.d.sync += [
                        self.sync.o.eq(1),
                        self.sdin.o.eq(0),
                        self.sclk.o.eq(0),
                        count.eq(24)
                ]
                m.d.comb += spi_done.eq(1)
                with m.If(spi_start):
                    m.d.comb += spi_done.eq(0)
                    m.d.sync += sr.eq(spi_data)
                    m.next = "SYNC_L"
            with m.State("SYNC_L"):
                with m.If(tick):
                    m.d.sync += self.sync.o.eq(0)
                    m.next = "CLK_H"
            with m.State("CLK_H"):
                with m.If(tick):
                    m.d.sync += self.sclk.o.eq(1)
                    m.d.sync += self.sdin.o.eq(sr[23])
                    m.d.sync += sr.eq(Cat(Const(0, unsigned(1)), sr))
                    m.d.sync += count.eq(count - 1)
                    m.next = "CLK_L"
            with m.State("CLK_L"):
                with m.If(tick):
                    m.d.sync += self.sclk.o.eq(0)
                    with m.If(count == 0):
                        m.next = "SYNC_H"
                    with m.Else():
                        m.next = "CLK_H"
            with m.State("SYNC_H"):
                with m.If(tick):
                    m.d.sync += self.sync.o.eq(1)
                    m.next = "WAIT"
            with m.State("WAIT"):
                with m.If(tick):
                    m.next = "IDLE"
        return m

