from amaranth import *

class HitSampler(Elaboratable):
    def __init__(self, fifo, trigger, shutter):
        self.fifo = fifo
        self.trigger = trigger
        self.shutter = shutter
        self.pos_ready = Signal()
        self.pos_x = Signal(16)
        self.pos_y = Signal(16)
        self.shutter_time = Signal(16)

    def elaborate(self, platform):
        m = Module()

        # prescaler to 10 µs time base
        # 32 bit timestamp accurate to 10 µs -> 12 hours of run time
        PRESCALE = 480
        tick = Signal()
        prescaler = Signal(range(PRESCALE))
        timestamp = Signal(32) 
        timestamp_trig = Signal(32)
        with m.If(prescaler != 0):
            m.d.sync += tick.eq(0)
            m.d.sync += prescaler.eq(prescaler - 1)
        with m.Else():
            m.d.sync += tick.eq(1)
            m.d.sync += prescaler.eq(PRESCALE - 1)
            m.d.sync += timestamp.eq(timestamp + 1)

        
        shutter_count = Signal(16)
        shutter_closed = Signal()
        # shutter control
        with m.FSM():
            with m.State("IDLE"):
                m.d.comb += shutter_closed.eq(0)
                with m.If(self.trigger):
                    m.d.sync += shutter_count.eq(self.shutter_time)
                    m.next = "WAIT_SHUTTER"
            with m.State("WAIT_SHUTTER"):
                m.d.comb += shutter_closed.eq(1)
                with m.If(tick):
                    m.d.sync += shutter_count.eq(shutter_count - 1)
                    with m.If(shutter_count == 0):
                        m.next = "IDLE"
        m.d.sync += self.shutter.eq(shutter_closed)
                
        # position sampling
        pos_x_ff = Signal(16)
        pos_y_ff = Signal(16)
        
        # FIFO transmission FSM
        with m.FSM():
            with m.State("IDLE"):
                # TODO: should we gate external events as long as we consider the shutter closed?
                with m.If(self.trigger & ~shutter_closed):
                    m.d.sync += timestamp_trig.eq(timestamp),
                    m.next = "WAIT_POS_READY"
            with m.State("WAIT_POS_READY"):
                with m.If(self.pos_ready):
                    m.d.sync += [
                        pos_x_ff.eq(self.pos_x),
                        pos_y_ff.eq(self.pos_y),
                    ]
                    m.next = "TX_TIMESTAMP0"
            with m.State("TX_TIMESTAMP0"):
                m.d.comb += self.fifo.w_en.eq(1)
                m.d.comb += self.fifo.w_data.eq(timestamp_trig[:8])
                m.next = "TX_TIMESTAMP1"
            with m.State("TX_TIMESTAMP1"):
                m.d.comb += self.fifo.w_en.eq(1)
                m.d.comb += self.fifo.w_data.eq(timestamp_trig[8:16])
                m.next = "TX_TIMESTAMP2"
            with m.State("TX_TIMESTAMP2"):
                m.d.comb += self.fifo.w_en.eq(1)
                m.d.comb += self.fifo.w_data.eq(timestamp_trig[16:24])
                m.next = "TX_TIMESTAMP3"
            with m.State("TX_TIMESTAMP3"):
                m.d.comb += self.fifo.w_en.eq(1)
                m.d.comb += self.fifo.w_data.eq(timestamp_trig[24:32])
                m.next = "TX_XL"
            with m.State("TX_XL"):
                m.d.comb += self.fifo.w_en.eq(1)
                m.d.comb += self.fifo.w_data.eq(pos_x_ff[:8])
                m.next = "TX_XH"
            with m.State("TX_XH"):
                m.d.comb += self.fifo.w_en.eq(1)
                m.d.comb += self.fifo.w_data.eq(pos_x_ff[8:16])
                m.next = "TX_YL"
            with m.State("TX_YL"):
                m.d.comb += self.fifo.w_en.eq(1)
                m.d.comb += self.fifo.w_data.eq(pos_y_ff[:8])
                m.next = "TX_YH"
            with m.State("TX_YH"):
                m.d.comb += self.fifo.w_en.eq(1)
                m.d.comb += self.fifo.w_data.eq(pos_y_ff[8:16])
                m.next = "IDLE"

        return m
