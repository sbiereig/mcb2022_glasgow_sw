from amaranth import *

from ....gateware.i2c import I2CInitiator

class I2CInterface(Elaboratable):
    def __init__(self, pads, addr_len):
        self.pads = pads
        self.launch = Signal(1)
        self.ack = Signal(1)
        self.slave_addr = Signal(7)
        self.reg_addr = Signal(16)
        self.reg_data = Signal(8)
        self.addr_len = addr_len

    def elaborate(self, platform):
        m = Module()

        m.submodules.i2c_initiator = i2c_initiator = I2CInitiator(
            pads=self.pads,
            period_cyc=480,
            clk_stretch=False
        )

        with m.FSM():
            with m.State("IDLE"):
                with m.If(~i2c_initiator.busy & self.launch):
                    m.next = "START_GEN"
            # generate start condition
            with m.State("START_GEN"):
                m.d.comb += i2c_initiator.start.eq(1)
                m.next = "START_WAIT"
            with m.State("START_WAIT"):
                with m.If(~i2c_initiator.busy):
                    m.next = "SLAVE_ADDR_GEN"
            # write slave address
            with m.State("SLAVE_ADDR_GEN"):
                m.d.comb += i2c_initiator.data_i.eq(Cat(C(0, 1), self.slave_addr))
                m.d.comb += i2c_initiator.write.eq(1)
                m.next = "SLAVE_ADDR_WAIT"
            with m.State("SLAVE_ADDR_WAIT"):
                with m.If(~i2c_initiator.busy):
                    with m.If(~i2c_initiator.ack_o):
                        m.d.sync += self.ack.eq(0)
                        m.next = "STOP_GEN"
                    with m.Else():
                        m.d.sync += self.ack.eq(1)
                        m.next = "REG_ADDR_GEN"
            # write register address
            with m.State("REG_ADDR_GEN"):
                if self.addr_len == 1:
                    m.d.comb += i2c_initiator.data_i.eq(self.reg_addr[0:8])
                elif self.addr_len == 2:
                    m.d.comb += i2c_initiator.data_i.eq(self.reg_addr[8:16])
                m.d.comb += i2c_initiator.write.eq(1)
                m.next = "REG_ADDR_WAIT"
            with m.State("REG_ADDR_WAIT"):
                with m.If(~i2c_initiator.busy):
                    with m.If(~i2c_initiator.ack_o):
                        m.d.sync += self.ack.eq(0)
                        m.next = "STOP_GEN"
                    with m.Else():
                        m.d.sync += self.ack.eq(1)
                        if self.addr_len == 1:
                            m.next = "REG_DATA_GEN"
                        elif self.addr_len == 2:
                            m.next = "REG_ADDR2_GEN"
            # write register address
            with m.State("REG_ADDR2_GEN"):
                m.d.comb += i2c_initiator.data_i.eq(self.reg_addr[0:8])
                m.d.comb += i2c_initiator.write.eq(1)
                m.next = "REG_ADDR2_WAIT"
            with m.State("REG_ADDR2_WAIT"):
                with m.If(~i2c_initiator.busy):
                    with m.If(~i2c_initiator.ack_o):
                        m.d.sync += self.ack.eq(0)
                        m.next = "STOP_GEN"
                    with m.Else():
                        m.d.sync += self.ack.eq(1)
                        m.next = "REG_DATA_GEN"
            # write register data
            with m.State("REG_DATA_GEN"):
                m.d.comb += i2c_initiator.data_i.eq(self.reg_data)
                m.d.comb += i2c_initiator.write.eq(1)
                m.next = "REG_DATA_WAIT"
            with m.State("REG_DATA_WAIT"):
                with m.If(~i2c_initiator.busy):
                    with m.If(~i2c_initiator.ack_o):
                        m.d.sync += self.ack.eq(0)
                    with m.Else():
                        m.d.sync += self.ack.eq(1)
                    m.next = "STOP_GEN"
            # generate stop condition
            with m.State("STOP_GEN"):
                m.d.comb += i2c_initiator.stop.eq(1)
                m.next = "STOP_WAIT"
            with m.State("STOP_WAIT"):
                with m.If(~i2c_initiator.busy & ~self.launch):
                    m.next = "IDLE"

        return m
