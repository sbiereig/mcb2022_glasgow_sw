"""LC DCO test chip driver"""

def _lc_ibias_encode(ibias):
    """Encodes the bias current setting"""
    if ibias < 0 or ibias > 24:
        raise ValueError("Invalid bias current setting")
    if ibias >= 16:
        return ibias + 8
    return ibias


def _lc_vbias_thermo(vbias):
    """Thermo-encodes and inverts bias voltage setting"""
    if vbias < 0 or vbias > 8:
        raise ValueError("Invalid bias voltage setting")
    bias_val = 0xFF
    bias_val = bias_val >> vbias
    return bias_val


def _lc_pvt_encode(bank):
    if bank >= 80:
        raise ValueError("Invalid PVT bank setting.")
    output = 0
    while bank >= 32:
        output = (output + 16) << 1
        bank = bank - 16
    output = output + bank
    return output


class LcDco:
    """LC DCO test chip driver class"""

    DCO_EN = 0x00
    DCO_BIAS_GM_DCO1 = 0x01
    DCO_BIAS_GM_BYPASS = 0x09
    DCO_BIAS_PVT_ACQ = 0x0A
    DCO_BIAS_TRK = 0x0B
    DCO_BANK_PVT = 0x0C
    DCO_BANK_ACQ = 0x0D
    DCO_BANK_TRK = 0x0E
    MUX_SEL_DCO = 0x0F
    MUX_SEL_DIV = 0x10
    MUX_SEL_DIV_OFF = 0
    MUX_SEL_DIV_40M = 1
    MUX_SEL_DIV_80M = 2
    MUX_SEL_DIV_160M = 3
    MUX_SEL_DIV_320M = 4
    MUX_SEL_DIV_640M = 5
    MUX_SEL_DIV_1280M = 6
    IO_DS = 0x11

    def __init__(self, write_spi):
        self._write_spi = write_spi

    async def write_reg(self, addr, val):
        frame = (addr << 9) + val
        await self._write_spi(frame)

    async def config_dco(
        self,
        num,
        bias_gm,
        bias_bypass,
        bank_pvt,
        bank_acq,
        bank_trk,
        mux_sel_div,
        bias_pvt_acq=4,
        bias_trk=4,
        bias_clkout=3,
    ):
        """
        Configures and enables one DCO at a time.
        Note: DCO number is zero-indexed (not starting with "DCO1" as some documentation indicates)
        """
        await self.write_reg(self.DCO_EN, 1 << num)
        for i in range(8):
            if i == num:
                await self.write_reg(self.DCO_BIAS_GM_DCO1 + i, _lc_ibias_encode(bias_gm))
            else:
                await self.write_reg(self.DCO_BIAS_GM_DCO1 + i, _lc_ibias_encode(0))
        await self.write_reg(self.DCO_BIAS_GM_BYPASS, bias_bypass << num)
        await self.write_reg(self.DCO_BIAS_PVT_ACQ, _lc_vbias_thermo(bias_pvt_acq))
        await self.write_reg(self.DCO_BIAS_TRK, _lc_vbias_thermo(bias_trk))
        await self.write_reg(self.DCO_BANK_PVT, _lc_pvt_encode(bank_pvt))
        await self.write_reg(self.DCO_BANK_ACQ, bank_acq)
        await self.write_reg(self.DCO_BANK_TRK, bank_trk)
        await self.write_reg(self.MUX_SEL_DCO, num)
        await self.write_reg(self.MUX_SEL_DIV, mux_sel_div)
        await self.write_reg(self.IO_DS, bias_clkout)
