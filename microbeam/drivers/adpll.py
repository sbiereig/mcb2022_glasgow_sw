"""ADPLL ASIC device abstraction"""

import time

from .adpll_constants import AdpllConstants

def _lc_ibias_encode(ibias):
    """Encodes the bias current setting"""
    if ibias < 0 or ibias > 24:
        raise ValueError("Invalid bias current setting")
    if ibias >= 16:
        return ibias + 8
    return ibias


def _lc_vbias_thermo(vbias):
    """Thermo-encodes and inverts bias voltage setting"""
    if vbias < 0 or vbias > 8:
        raise ValueError("Invalid bias voltage setting")
    bias_val = 0xFF
    bias_val = bias_val >> vbias
    return bias_val


def _lc_pvt_encode(bank):
    if bank >= 80:
        raise ValueError("Invalid PVT bank setting.")
    output = 0
    while bank >= 32:
        output = (output + 16) << 1
        bank = bank - 16
    output = output + bank
    return output


def _clip(val, val_min, val_max):
    if val < val_min:
        return val_min
    if val > val_max:
        return val_max
    return val


class Adpll(AdpllConstants):
    """ADPLL ASIC device abstraction class"""

    # pylint: disable=too-many-public-methods

    I2C_TRIES = 3

    def __init__(self, i2c_write, logger, chip_address=0x40):
        self.i2c_write = i2c_write
        self.logger = logger
        self.chip_address = chip_address

    async def write_reg_i2c(self, address, data):
        """Writes register(s) via I2C interface."""
        ack = False
        for tries in range(self.I2C_TRIES):
            ack = await self.i2c_write(
                slave_addr=self.chip_address,
                reg_addr=address,
                reg_data=data,
            )
            if ack:
                return
        raise RuntimeError("ADPLL did not acknowledge!")

    async def read_reg_i2c(self, address):
        """Reads a single register"""
        values = await self.read_regs_i2c(address=address, read_len=1)
        return values[0]

    async def read_regs_i2c(self, address, read_len=1):
        """Reads multiple registers - stubbed out currently"""
        return [0] * read_len

    async def setup_i2c(
        self,
        clk_dis=None,
        clken_force=False,
        sda_drive=False,
    ):
        """Writes the I2C clock/control register"""
        clk_dis = clk_dis or [False, False, False]
        if len(clk_dis) != 3:
            raise ValueError("Invalid value for clock disable register")
        reg_val = (
            clk_dis[0] << self.I2CCLKCTRL_CLKADISABLE_of
            | clk_dis[1] << self.I2CCLKCTRL_CLKBDISABLE_of
            | clk_dis[2] << self.I2CCLKCTRL_CLKCDISABLE_of
            | clken_force << self.I2CCLKCTRL_FORCECLKENABLE_of
            | sda_drive << self.I2CCLKCTRL_I2CDRIVESDA_of
        )
        await self.write_reg_i2c(self.I2CCLKCTRL, reg_val)

    def _io_pll_rx_regval(self, reg_name, rx_en, term_en, bias):
        """Returns an 8 bit register value, pre-filled with RX related bits"""
        if bias < 0 or bias > 15:
            raise ValueError("Invalid RX bias current value")

        reg_val = (
            bool(rx_en) << getattr(self, reg_name + "_RX_EN_of")
            | bool(term_en) << getattr(self, reg_name + "_TERM_EN_of")
            | bias << getattr(self, reg_name + "_BIAS_of")
        )
        return reg_val

    def _io_mseu_rx_regval(self, reg_name, rx_en, term_en, bias):
        """Returns an 8 bit register value, pre-filled with RX related bits"""
        if bias < 0 or bias > 15:
            raise ValueError("Invalid RX bias current value")

        reg_val = (
            bool(rx_en) << getattr(self, reg_name + "_RX_EN_of")
            | bool(term_en) << getattr(self, reg_name + "_RX_TERM_EN_of")
            | bias << getattr(self, reg_name + "_RX_BIAS_of")
        )
        return reg_val

    async def setup_io_refclk(self, pll_core, rx_en=True, term_en=True, bias=7):
        """Configures reference clock receiver inputs"""
        reg_name = "IO_" + pll_core + "_REFCLK"
        reg_val = self._io_pll_rx_regval(reg_name, rx_en, term_en, bias)
        await self.write_reg_i2c(getattr(self, reg_name), reg_val)

    async def setup_io_din(self, pll_core, rx_en=True, term_en=True, bias=7):
        """Configures data receiver inputs (unused)"""
        reg_name = "IO_" + pll_core + "_DIN"
        reg_val = self._io_pll_rx_regval(reg_name, rx_en, term_en, bias)
        await self.write_reg_i2c(getattr(self, reg_name), reg_val)

    async def setup_io_clkout(self, pll_core, clkout_bias, tstout_bias):
        """Sets up all PLL core outputs"""
        if len(clkout_bias) != 3:
            raise ValueError("Invalid number of CLKOUT bias values")
        for bias in clkout_bias:
            if bias < 0 or bias > 3:
                raise ValueError("Invalid TX bias current value")
        if tstout_bias < 0 or tstout_bias > 3:
            raise ValueError("Invalid TX bias current value")

        reg_name = "IO_" + pll_core + "_OUT"
        reg_val = (
            clkout_bias[0] << getattr(self, reg_name + "_CLKOUT0_BIAS_of")
            | clkout_bias[1] << getattr(self, reg_name + "_CLKOUT1_BIAS_of")
            | clkout_bias[2] << getattr(self, reg_name + "_CLKOUT2_BIAS_of")
            | tstout_bias << getattr(self, reg_name + "_TSTOUT_BIAS_of")
        )
        await self.write_reg_i2c(getattr(self, reg_name), reg_val)

    async def setup_io_mseu(self, mseu_name, rx_en=True, term_en=True, rx_bias=7, tx_bias=3):
        """Sets up IO pins related to the MSEU blocks"""
        # pylint: disable=too-many-arguments
        reg_name = "IO_" + mseu_name
        reg_val = self._io_mseu_rx_regval(reg_name, rx_en, term_en, rx_bias)
        reg_val |= tx_bias << getattr(self, reg_name + "_TX_BIAS_of")
        await self.write_reg_i2c(getattr(self, reg_name), reg_val)

    async def setup_io_default(self, loop_core, rx_term=True, rx_bias=3, tx_bias=3):
        """Sets up default I/O configurations for one PLL core"""
        await self.setup_io_refclk(loop_core, rx_en=True, term_en=rx_term, bias=rx_bias)
        await self.setup_io_din(loop_core, rx_en=False, term_en=False, bias=0)
        await self.setup_io_clkout(loop_core, clkout_bias=[tx_bias] * 3, tstout_bias=tx_bias)

    async def pll_reset(self, loop_core, reset_dlf=True, reset_sdm=True):
        """Reset PLL loop components"""
        reg_name = "PLL_" + loop_core + "_CTRL0"
        reg_val = await self.read_reg_i2c(getattr(self, reg_name))
        if reset_dlf:
            reg_val &= ~(1 << getattr(self, reg_name + "_DLF_NRST_of"))
        else:
            reg_val |= 1 << getattr(self, reg_name + "_DLF_NRST_of")
        if reset_sdm:
            reg_val &= ~(1 << getattr(self, reg_name + "_DLF_SDMNRST_of"))
        else:
            reg_val |= 1 << getattr(self, reg_name + "_DLF_SDMNRST_of")
        await self.write_reg_i2c(getattr(self, reg_name), reg_val)

    async def setup_pll_basic(
        self, loop_core, cdr=False, pd_sel="DFQD1LVT", pd_reg=False, pd_rising=False
    ):
        """Basic setup for PLL mode"""
        # pylint: disable=too-many-arguments
        if pd_sel not in [
            "DFQD1LVT",
            "DFQD1LVT_NOCG",
            "DFQD1LVT_COMB",
            "DFQD1LVT_DUAL",
        ]:
            raise ValueError("Invalid PD selection")
        reg_name = "PLL_" + loop_core + "_CTRL0"
        reg_val = await self.read_reg_i2c(getattr(self, reg_name))
        reg_val &= ~(3 << getattr(self, reg_name + "_PD_SEL_of"))
        reg_val &= ~(7 << getattr(self, reg_name + "_MODE_SEL_of"))
        reg_val |= getattr(self, reg_name + "_PD_SEL_" + pd_sel) << getattr(
            self, reg_name + "_PD_SEL_of"
        )
        mode_str = reg_name + "_MODE_SEL_"
        mode_str += "CDR_" if cdr else "PLL_"
        mode_str += "REGPD" if pd_reg else "NORM"
        if cdr:
            mode_str += "_RISING" if pd_rising else "_EITHER"
        reg_val |= getattr(self, mode_str) << getattr(self, reg_name + "_MODE_SEL_of")
        await self.write_reg_i2c(getattr(self, reg_name), reg_val)

    async def setup_pll_tstout(self, loop_core, enable=True, prbs=False, scrambler=True):
        """Sets up the serializer output configuration"""
        reg_name = "PLL_" + loop_core + "_CTRL1"
        reg_val = await self.read_reg_i2c(getattr(self, reg_name))

        if enable:
            reg_val |= 1 << getattr(self, reg_name + "_SER_EN_of")
        else:
            reg_val &= ~(1 << getattr(self, reg_name + "_SER_EN_of"))

        if prbs:
            reg_val |= 1 << getattr(self, reg_name + "_SER_PRBS_EN_of")
        else:
            reg_val &= ~(1 << getattr(self, reg_name + "_SER_PRBS_EN_of"))

        if scrambler:
            reg_val &= ~(1 << getattr(self, reg_name + "_SER_SCRAMBLER_BYPASS_of"))
        else:
            reg_val |= 1 << getattr(self, reg_name + "_SER_SCRAMBLER_BYPASS_of")
        await self.write_reg_i2c(getattr(self, reg_name), reg_val)

    async def setup_pll_clock(self, loop_core, ref_freq_str, sdm_freq_str):
        """Set up PLL clocking settings"""
        if ref_freq_str not in ["40M", "80M", "160M", "320M"]:
            raise ValueError("Invalid reference clock frequency")
        if sdm_freq_str not in ["OFF", "160M", "320M", "640M"]:
            raise ValueError("Invalid SDM clock frequency")

        reg_name = "PLL_" + loop_core + "_CLKCTRL0"
        reg_val = await self.read_reg_i2c(getattr(self, reg_name))
        reg_val &= ~(3 << getattr(self, reg_name + "_REF_FREQ_SEL_of"))
        reg_val &= ~(3 << getattr(self, reg_name + "_SDM_FREQ_SEL_of"))
        reg_val |= getattr(self, reg_name + "_REF_FREQ_SEL_" + ref_freq_str) << getattr(
            self, reg_name + "_REF_FREQ_SEL_of"
        )
        reg_val |= getattr(self, reg_name + "_SDM_FREQ_SEL_" + sdm_freq_str) << getattr(
            self, reg_name + "_SDM_FREQ_SEL_of"
        )
        await self.write_reg_i2c(getattr(self, reg_name), reg_val)

    async def setup_pll_clkout(self, loop_core, clkout, clkout_freq_str):
        """Configures PLL clock output settings"""
        if clkout < 0 or clkout > 2:
            raise ValueError("Invalid Clock Output ID")
        if clkout_freq_str not in [
            "OFF",
            "40M",
            "80M",
            "160M",
            "320M",
            "640M",
            "1280M",
            "DOUT",
        ]:
            raise ValueError("Invalid CLKOUT configuration.")

        if clkout == 0:
            reg_name = "PLL_" + loop_core + "_CLKCTRL0"
        else:
            reg_name = "PLL_" + loop_core + "_CLKCTRL1"
        reg_val = await self.read_reg_i2c(getattr(self, reg_name))
        reg_val &= ~(7 << getattr(self, reg_name + "_CLKOUT" + str(clkout) + "_SEL_of"))
        reg_val |= getattr(
            self, reg_name + "_CLKOUT" + str(clkout) + "_SEL_" + clkout_freq_str
        ) << getattr(self, reg_name + "_CLKOUT" + str(clkout) + "_SEL_of")
        await self.write_reg_i2c(getattr(self, reg_name), reg_val)

    async def setup_pll_dlf(self, loop_core, dlf_kp, dlf_ki):
        """Configure PLL loop filter gain"""
        if dlf_kp < 0 or dlf_kp > 15:
            raise ValueError("Invalid value for Kp.")
        if dlf_ki < 0 or dlf_ki > 15:
            raise ValueError("Invalid value for Ki.")

        reg_name = "PLL_" + loop_core + "_DLF_CONFIG0"
        reg_val = (dlf_ki << getattr(self, reg_name + "_KI_of")) + (
            dlf_kp << getattr(self, reg_name + "_KP_of")
        )
        await self.write_reg_i2c(getattr(self, reg_name), reg_val)

    async def setup_pll_dlf_preset(self, loop_core, preset):
        """Configure DLF integrator preset value"""
        if preset < 0 or preset > 2 ** 16 - 1:
            raise ValueError("Invalid preset value")
        lsb_reg_name = "PLL_" + loop_core + "_DLF_CONFIG1"
        msb_reg_name = "PLL_" + loop_core + "_DLF_CONFIG2"
        await self.write_reg_i2c(getattr(self, msb_reg_name), preset >> 8)
        await self.write_reg_i2c(getattr(self, lsb_reg_name), preset & 0xFF)

    async def _setup_ro_dco(self, loop_core, config):
        if "enable" not in config:
            raise ValueError("Missing DCO enable value.")
        if "ftw_offset" not in config:
            raise ValueError("Missing DCO FTW offset value.")
        if config["ftw_offset"] < 0 or config["ftw_offset"] > 62:
            raise ValueError("Invalid DCO FTW offset value.")
        reg_name = "PLL_" + loop_core + "_DCO_CTRL"
        reg_val = int(config["enable"]) << getattr(self, reg_name + "_DCO_EN_of")
        reg_val |= int(config["ftw_offset"]) << getattr(
            self, reg_name + "_DCO_FTW_OFFS_of"
        )
        await self.write_reg_i2c(getattr(self, reg_name), reg_val)

    async def _setup_lc_dco(self, loop_core, config):
        if "ibias" in config or "ibias_bypass" in config:
            if "ibias" not in config:
                raise ValueError("Bias current value not provided.")
            if "ibias_bypass" not in config:
                raise ValueError("Bias bypass value not provided.")
            reg_name = "PLL_" + loop_core + "_DCO_CTRL0"
            reg_val = int(config["ibias_bypass"]) << getattr(
                self, reg_name + "_IBIAS_BYPASS_of"
            )
            ibias_setting = _lc_ibias_encode(config["ibias"])
            reg_val |= ibias_setting << getattr(self, reg_name + "_IBIAS_of")
            await self.write_reg_i2c(getattr(self, reg_name), reg_val)
        if "vbias_pvt_acq" in config:
            reg_name = "PLL_" + loop_core + "_DCO_CTRL1"
            reg_val = _lc_vbias_thermo(config["vbias_pvt_acq"])
            await self.write_reg_i2c(getattr(self, reg_name), reg_val)
        if "vbias_trk" in config:
            reg_name = "PLL_" + loop_core + "_DCO_CTRL2"
            reg_val = _lc_vbias_thermo(config["vbias_trk"])
            await self.write_reg_i2c(getattr(self, reg_name), reg_val)
        if "pvt" in config:
            reg_name = "PLL_" + loop_core + "_DCO_CTRL3"
            reg_val = _lc_pvt_encode(config["pvt"])
            await self.write_reg_i2c(getattr(self, reg_name), reg_val)
        if "acq" in config:
            reg_name = "PLL_" + loop_core + "_DCO_CTRL4"
            if config["acq"] < 0 or config["acq"] > 63:
                raise ValueError("Invalid ACQ bank setting.")
            reg_val = config["acq"]
            await self.write_reg_i2c(getattr(self, reg_name), reg_val)

    async def setup_pll_dco(self, loop_core, config):
        """Configuration of loop DCO parameters."""
        if not isinstance(config, dict):
            raise ValueError("Invalid DCO configuration options argument.")
        if loop_core in ["SRO", "TRO"]:
            await self._setup_ro_dco(loop_core, config)
        elif loop_core == "LC":
            await self._setup_lc_dco(loop_core, config)
        else:
            raise ValueError("Invalid loop core name")

    # Acquisition-FSM related functionality
    async def setup_acq_fsm(self, loop_core, fref_str, cdr=False, res="max"):
        """Sets up acquisition FSM parameters

        The FSM itself is clocked using a feedback 40 MHz clock.
        End-of-count settings can be altered to align the counter race
        speed and trade off accuracy for speed.

        Counter length: 8 bit + EOC-setting.
        """
        if fref_str not in ["40M", "80M", "160M", "320M"]:
            raise ValueError("Invalid reference frequency")
        if res not in ["min", "max"]:
            raise ValueError("Invalid acquisition resolution")

        fref_diff_map = {"40M": 0, "80M": 1, "160M": 2, "320M": 3}
        eoc_diff = fref_diff_map[fref_str] - 2 * int(cdr)

        if res == "max":  # maximize EOC values
            if eoc_diff < 0:
                eoc_dco = 7
                eoc_ref = 7 + eoc_diff
            else:
                eoc_dco = 7 - eoc_diff
                eoc_ref = 7
        elif res == "min":  # minimize EOC values
            if eoc_diff < 0:
                eoc_dco = 0 - eoc_diff
                eoc_ref = 0
            else:
                eoc_dco = 0
                eoc_ref = 0 + eoc_diff
        assert 0 <= eoc_dco <= 7
        assert 0 <= eoc_ref <= 7

        reg_name = "PLL_" + loop_core + "_ACQ_CTRL"
        reg_val = 0
        reg_val |= eoc_ref << getattr(self, reg_name + "_ACQ_EOC_REF_of")
        reg_val |= eoc_dco << getattr(self, reg_name + "_ACQ_EOC_DCO_of")
        await self.write_reg_i2c(getattr(self, reg_name), reg_val)

    async def run_acq_fsm(self, loop_core):
        """Runs the acquisition FSM, returns True is faster than reference clock"""
        ctrl_reg_name = "PLL_" + loop_core + "_ACQ_CTRL"
        res_reg_name = "PLL_" + loop_core + "_FSM_RESULT"
        ctrl_reg_val = await self.read_reg_i2c(getattr(self, ctrl_reg_name))
        # set start bit
        ctrl_reg_val_start = ctrl_reg_val | 1 << getattr(
            self, ctrl_reg_name + "_ACQ_START_of"
        )
        await self.write_reg_i2c(getattr(self, ctrl_reg_name), ctrl_reg_val_start)
        tries = 0
        res_reg_val = 0
        while not res_reg_val & (1 << getattr(self, res_reg_name + "_ACQ_DONE_of")):
            tries += 1
            res_reg_val = await self.read_reg_i2c(getattr(self, res_reg_name))
            if tries > 10:
                # clear start bit
                await self.write_reg_i2c(getattr(self, ctrl_reg_name), ctrl_reg_val)
                raise RuntimeError("Acquisition FSM did not finish.")
        # clear start bit
        await self.write_reg_i2c(getattr(self, ctrl_reg_name), ctrl_reg_val)
        # calculate result
        dco_fast = bool(
            res_reg_val & (1 << getattr(self, res_reg_name + "_ACQ_DCO_FAST_of"))
        )
        return dco_fast

    async def _run_lc_acq_loop(self, loop_core):
        selected_params = {}
        pvt = 40
        acq = 32
        await self.setup_pll_dco(loop_core, {"pvt": pvt, "acq": acq})

        # PVT binary search
        pvt_step_sizes = [20, 16, 8, 4, 2, 1]
        for step_size in pvt_step_sizes:
            dco_fast = await self.run_acq_fsm(loop_core)
            self.logger.debug("PVT: %d, DCO fast: %d", pvt, dco_fast)
            if dco_fast:
                pvt = _clip(pvt + step_size, 0, 79)
            else:
                pvt = _clip(pvt - step_size, 0, 79)
            await self.setup_pll_dco(loop_core, {"pvt": pvt})
        dco_fast = await self.run_acq_fsm(loop_core)
        self.logger.debug("PVT: %d, DCO fast: %d", pvt, dco_fast)
        if dco_fast:
            pvt = _clip(pvt + 1, 0, 79)
            await self.setup_pll_dco(loop_core, {"pvt": pvt})
            dco_fast = await self.run_acq_fsm(loop_core)
            self.logger.debug("PVT: %d, DCO fast: %d", pvt, dco_fast)

        selected_params["pvt"] = pvt

        # ACQ binary search
        acq_step_sizes = [16, 8, 4, 2, 1]
        for step_size in acq_step_sizes:
            dco_fast = await self.run_acq_fsm(loop_core)
            self.logger.debug("ACQ: %d, DCO fast: %d", acq, dco_fast)
            if not dco_fast:
                acq = _clip(acq + step_size, 0, 63)
            else:
                acq = _clip(acq - step_size, 0, 63)
            await self.setup_pll_dco(loop_core, {"acq": acq})
        dco_fast = await self.run_acq_fsm(loop_core)
        self.logger.debug("ACQ: %d, DCO fast: %d", acq, dco_fast)
        if not dco_fast:
            acq = _clip(acq + 1, 0, 63)
            await self.setup_pll_dco(loop_core, {"acq": acq})
            dco_fast = await self.run_acq_fsm(loop_core)
            self.logger.debug("ACQ: %d, DCO fast: %d", acq, dco_fast)
        selected_params["acq"] = acq
        return selected_params

    async def _run_ro_acq_loop(self, loop_core):
        ftw_offset = 0
        dlf_max = False  # we had to fall back to forcing the DLF high
        # incremental search
        incr_step_size = 8
        dco_fast = False
        while not dco_fast:
            ftw_offset += incr_step_size
            ftw_offset = min(ftw_offset, 62)
            # potential hazard if DLF centering is not sufficient for max freq (fix for TID!)
            await self.setup_pll_dco(loop_core, {"enable": 1, "ftw_offset": ftw_offset})
            tries = 0
            while True:
                try:
                    dco_fast = await self.run_acq_fsm(loop_core)
                except RuntimeError:
                    tries += 1
                else:
                    break
                if tries >= 10:
                    raise RuntimeError("Acquisition failed.")
            self.logger.debug("FTW offset: %d, DCO fast: %d", ftw_offset, dco_fast)
            if ftw_offset == 62 and dlf_max and not dco_fast:
                raise RuntimeError("DCO is too slow for successful acquisition.")
            if ftw_offset == 62 and not dco_fast:
                await self.setup_pll_dlf_preset(loop_core, int(2 ** 16) - 1)
                dlf_max = True

        # binary search
        ftw_offset = _clip(ftw_offset - int(incr_step_size / 2), 0, 62)
        await self.setup_pll_dco(loop_core, {"enable": 1, "ftw_offset": ftw_offset})
        step_sizes = [4, 2, 1]
        for step_size in step_sizes:
            dco_fast = await self.run_acq_fsm(loop_core)
            self.logger.debug("FTW offset: %d, DCO fast: %d", ftw_offset, dco_fast)
            if not dco_fast:
                ftw_offset = _clip(ftw_offset + step_size, 0, 62)
            else:
                ftw_offset = _clip(ftw_offset - step_size, 0, 62)
            await self.setup_pll_dco(loop_core, {"enable": 1, "ftw_offset": ftw_offset})
        dco_fast = await self.run_acq_fsm(loop_core)
        self.logger.debug("FTW offset: %d, DCO fast: %d", ftw_offset, dco_fast)
        if not dco_fast:
            ftw_offset = _clip(ftw_offset + 1, 0, 62)
            await self.setup_pll_dco(loop_core, {"enable": 1, "ftw_offset": ftw_offset})
            dco_fast = await self.run_acq_fsm(loop_core)
            self.logger.debug("FTW offset: %d, DCO fast: %d", ftw_offset, dco_fast)

        return {"ftw_offset": ftw_offset}

    async def run_acq_loop(self, loop_core):
        """Runs a full acquisition sequence for a given loop core.
        Assumes that the acquisition FSM has been configured before.

        Integrator is assumed to be held at mid-scale.
        """
        if loop_core == "LC":
            return await self._run_lc_acq_loop(loop_core)
        if loop_core in ["SRO", "TRO"]:
            return await self._run_ro_acq_loop(loop_core)
        return {}

    # Autocorrelation-FSM related functionality
    async def setup_acorr_fsm(self, loop_core, lag):
        """Sets up the PD autocorrelation lag"""
        if lag > 7:
            raise ValueError("Invalid Autocorrelator Lag")
        reg_name = "PLL_" + loop_core + "_CTRL1"
        reg_val = await self.read_reg_i2c(getattr(self, reg_name))
        reg_val &= ~(7 << getattr(self, reg_name + "_ACORR_LAG_of"))
        reg_val |= lag << getattr(self, reg_name + "_ACORR_LAG_of")
        await self.write_reg_i2c(getattr(self, reg_name), reg_val)

    async def run_acorr_fsm(self, loop_core):
        """Runs the autocorrelation FSM, returns the normalized autocorrelation"""
        ctrl_reg_name = "PLL_" + loop_core + "_CTRL1"
        res_reg_name = "PLL_" + loop_core + "_FSM_RESULT"
        ctrl_reg_val = await self.read_reg_i2c(getattr(self, ctrl_reg_name))
        # set start bit
        ctrl_reg_val_start = ctrl_reg_val | (
            1 << getattr(self, ctrl_reg_name + "_ACORR_START_of")
        )
        await self.write_reg_i2c(getattr(self, ctrl_reg_name), ctrl_reg_val_start)
        # clear start bit (busy waiting is buggy, assume it's done)
        await self.write_reg_i2c(getattr(self, ctrl_reg_name), ctrl_reg_val)
        time.sleep(0.01)
        res_reg_val = await self.read_reg_i2c(getattr(self, res_reg_name))
        # calculate result
        acorr = 2 ** (
            (res_reg_val >> getattr(self, res_reg_name + "_ACORR_MAG_of")) & 0x0F
        )
        if res_reg_val & (1 << getattr(self, res_reg_name + "_ACORR_SIGN_of")):
            acorr = -acorr
        return acorr

    # SEU counter related functionality
    async def read_seu_count(self):
        """Read out SEU counter registers, return as 32 bit number"""
        seu_count = 0
        seu_cnt_regval = await self.read_regs_i2c(self.SEUCNT0, read_len=4)
        for i in range(4):
            seu_count += seu_cnt_regval[i] << (8 * i)
        return seu_count
