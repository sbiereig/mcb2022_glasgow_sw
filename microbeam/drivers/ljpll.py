from .ljpll_constants import LjpllConstants

class Ljpll(LjpllConstants):
    """Use flow:
      1. create object
      2. call any of the methods for configuration setup
      3. call "program" to actually perform the SPI write
    """
    # general questions: 
    # - reset polarity - it appears to be active high!
    # - frequency tuning range of this chip? does it cover 2.56 GHz?
    # - expected CDAC tuning range? appears very small (<1%!)
    def __init__(self, spi_write):
        self._frame = 0
        self._spi_write = spi_write

    async def program(self):
        await self._spi_write(self._frame)

    def set_global_irx(self, enable=True, dac=16, ring_irx_conf=0, lc_irx_conf=0):
        # question: what does the input RX config do?
        assert dac in range(32)
        assert ring_irx_conf in range(4)
        assert lc_irx_conf in range(4)
        self._frame += (enable << self.GLOBAL_IRXCE_of)
        self._frame += (dac << self.GLOBAL_IRXDACSET_of)
        self._frame += (ring_irx_conf << self.RING_IRXCONF_of)
        self._frame += (lc_irx_conf << self.LC_IRXCONF_of)

    def set_global_abuff(self, enable=True, dac=16):
        assert dac in range(32)
        self._frame += (enable << self.GLOBAL_ABUFFCE_of)
        self._frame += (dac << self.GLOBAL_ABUFFDACSET_of)

    def set_global_obuff(self, enable=False, dac=16):
        assert dac in range(32)
        self._frame += (enable << self.GLOBAL_OBUFFCE_of)
        self._frame += (dac << self.GLOBAL_OBUFFDAC_of)

    def set_global_buffen(self, buff0=False, buff1=False, buff2=False, buff3=False):
        self._frame += ((not buff0) << self.GLOBAL_NBUFF0EN_of)
        self._frame += ((not buff1) << self.GLOBAL_NBUFF1EN_of)
        self._frame += ((not buff2) << self.GLOBAL_NBUFF2EN_of)
        self._frame += ((not buff3) << self.GLOBAL_NBUFF3EN_of)

    def set_lc_pll(self,
        pd_enable=False,
        cp_enable=False,
        dac=64,
        iref=16,
        mmcomp=4,
        mmdir=0,
        resistor=16
    ):
        assert dac in range(128)
        assert iref in range(32)
        assert mmcomp in range(8)
        assert resistor in range(32)

        self._frame += (pd_enable << self.LC_PD_ENSPFD_of)
        self._frame += (cp_enable << self.LC_CP_CE_of)
        self._frame += (dac << self.LC_CP_DACSET_of)
        self._frame += (iref << self.LC_CP_IREFCPSET_of)
        self._frame += (mmcomp << self.LC_CP_MMCOMP_of)
        self._frame += (mmdir << self.LC_CP_MMDIR_of)
        self._frame += (resistor << self.LC_RESISTOR_of)

    def set_lc_vco(self,
        bias_en=True,
        dac=64,
        igen_start=True,
        capsel=32,
        railmode=False,  # railmode=True == current mode
        vtune_ext=False,
        afc=True
    ):
        assert dac in range(128)
        assert capsel in range(7)

        capsel_tmp = 0
        for i in range(capsel):
            capsel_tmp += (1 << i)
        assert capsel_tmp in range(64)


        self._frame += (bias_en << self.LC_VCO_BE_of)
        self._frame += (dac << self.LC_VCO_DACSET_of)
        self._frame += (igen_start << self.LC_VCO_IGEN_START_of)
        self._frame += (capsel_tmp << self.LC_VCO_CAPSEL_of)
        self._frame += (railmode << self.LC_VCO_NRAILMODE_of)
        self._frame += (vtune_ext << self.LC_VCO_SW_EXT_of)
        self._frame += (afc << self.LC_VCO_SW_AFC_of)

    def set_lc_skip(self, skipa=False, skipb=False, skipc=False):
        self._frame += (skipa << self.LC_DIV_SKIPA_of)
        self._frame += (skipb << self.LC_DIV_SKIPB_of)
        self._frame += (skipc << self.LC_DIV_SKIPC_of)








