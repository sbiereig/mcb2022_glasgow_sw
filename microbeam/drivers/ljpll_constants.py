"""ljPLL register map"""


class LjpllConstants:
    """ljPLL Constant Wrapper class"""

    SR_BITS = 92

    RING_CP_CE_of = 0
    RING_CP_DACSET_of = 1
    RING_CP_IREFCPSET_of = 8
    RING_PD_ENSPFD_of = 13
    RING_RESISTOR_of = 14
    RING_VCO_SW_EXT_of = 19
    RING_IRXCONF_of = 20

    GLOBAL_IRXDACSET_of = 22
    GLOBAL_IRXCE_of = 27
    GLOBAL_ABUFFDACSET_of = 28
    GLOBAL_ABUFFCE_of = 33
    
    LC_CP_CE_of = 34
    LC_CP_DACSET_of = 35
    LC_CP_IREFCPSET_of = 42
    LC_CP_MMCOMP_of = 47
    LC_CP_MMDIR_of = 50
    LC_PD_ENSPFD_of = 51
    LC_RESISTOR_of = 52
    LC_VCO_SW_EXT_of = 57
    LC_VCO_DACSET_of = 59
    LC_VCO_BE_of = 66
    LC_VCO_IGEN_START_of = 67
    LC_VCO_NRAILMODE_of = 68
    LC_IRXCONF_of = 69

    GLOBAL_OBUFFDAC_of = 71
    GLOBAL_OBUFFCE_of = 76
    GLOBAL_NBUFF0EN_of = 77
    GLOBAL_NBUFF1EN_of = 78
    GLOBAL_NBUFF2EN_of = 79
    GLOBAL_NBUFF3EN_of = 80

    LC_VCO_CAPSEL_of = 81
    LC_VCO_SW_AFC_of = 87
    LC_DIV_SKIPA_of = 88
    LC_DIV_SKIPB_of = 89
    LC_DIV_SKIPC_of = 90
