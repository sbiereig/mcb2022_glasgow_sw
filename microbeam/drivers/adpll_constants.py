"""ADPLL Register map"""


class AdpllConstants:
    """ADPLL Constant Wrapper class"""

    # pylint: disable=too-few-public-methods

    REGMAP_RW_REGS = 43
    REGMAP_RO_REGS = 7

    # Register 0 (I2CCLKCTRL) - I2C clock control register
    I2CCLKCTRL = 0x0000
    I2CCLKCTRL_CLKADISABLE_of = 0  # Disable clock A
    I2CCLKCTRL_CLKBDISABLE_of = 1  # Disable clock B
    I2CCLKCTRL_CLKCDISABLE_of = 2  # Disable clock C
    I2CCLKCTRL_FORCECLKENABLE_of = 3  # Permanently enable I2C clock generator
    I2CCLKCTRL_I2CDRIVESDA_of = 4  # Drive SDA

    # Register 1 (IO_SRO_REFCLK) - Single RO PLL reference clock input pad control register
    IO_SRO_REFCLK = 0x0001
    IO_SRO_REFCLK_RX_EN_of = 0  # Input receiver enable
    IO_SRO_REFCLK_TERM_EN_of = 1  # Input termination enable
    IO_SRO_REFCLK_BIAS_of = 2  # Receiver bias current

    # Register 2 (IO_SRO_DIN) - Single RO PLL Data input pad control register
    IO_SRO_DIN = 0x0002
    IO_SRO_DIN_RX_EN_of = 0  # Input receiver enable
    IO_SRO_DIN_TERM_EN_of = 1  # Input termination enable
    IO_SRO_DIN_BIAS_of = 2  # Receiver bias current

    # Register 3 (IO_SRO_OUT) - Single RO PLL output pad control register
    IO_SRO_OUT = 0x0003
    IO_SRO_OUT_CLKOUT0_BIAS_of = 0  # Clock output 0 bias current
    IO_SRO_OUT_CLKOUT1_BIAS_of = 2  # Clock output 1 bias current
    IO_SRO_OUT_CLKOUT2_BIAS_of = 4  # Clock output 2 bias current
    IO_SRO_OUT_TSTOUT_BIAS_of = 6  # Serializer output bias current

    # Register 4 (IO_TRO_REFCLK) - Triplicated RO PLL reference clock input pad control register
    IO_TRO_REFCLK = 0x0004
    IO_TRO_REFCLK_RX_EN_of = 0  # Input receiver enable
    IO_TRO_REFCLK_TERM_EN_of = 1  # Input termination enable
    IO_TRO_REFCLK_BIAS_of = 2  # Receiver bias current

    # Register 5 (IO_TRO_DIN) - Triplicated RO PLL Data input pad control register
    IO_TRO_DIN = 0x0005
    IO_TRO_DIN_RX_EN_of = 0  # Input receiver enable
    IO_TRO_DIN_TERM_EN_of = 1  # Input termination enable
    IO_TRO_DIN_BIAS_of = 2  # Receiver bias current

    # Register 6 (IO_TRO_OUT) - Triplicated RO PLL output pad control register
    IO_TRO_OUT = 0x0006
    IO_TRO_OUT_CLKOUT0_BIAS_of = 0  # Clock output 0 bias current
    IO_TRO_OUT_CLKOUT1_BIAS_of = 2  # Clock output 1 bias current
    IO_TRO_OUT_CLKOUT2_BIAS_of = 4  # Clock output 2 bias current
    IO_TRO_OUT_TSTOUT_BIAS_of = 6  # Serializer output bias current

    # Register 7 (IO_LC_REFCLK) - LC PLL reference clock input pad control register
    IO_LC_REFCLK = 0x0007
    IO_LC_REFCLK_RX_EN_of = 0  # Input receiver enable
    IO_LC_REFCLK_TERM_EN_of = 1  # Input termination enable
    IO_LC_REFCLK_BIAS_of = 2  # Receiver bias current

    # Register 8 (IO_LC_DIN) - LC PLL Data input pad control register
    IO_LC_DIN = 0x0008
    IO_LC_DIN_RX_EN_of = 0  # Input receiver enable
    IO_LC_DIN_TERM_EN_of = 1  # Input termination enable
    IO_LC_DIN_BIAS_of = 2  # Receiver bias current

    # Register 9 (IO_LC_OUT) - LC PLL output pad control register
    IO_LC_OUT = 0x0009
    IO_LC_OUT_CLKOUT0_BIAS_of = 0  # Clock output 0 bias current
    IO_LC_OUT_CLKOUT1_BIAS_of = 2  # Clock output 1 bias current
    IO_LC_OUT_CLKOUT2_BIAS_of = 4  # Clock output 2 bias current
    IO_LC_OUT_TSTOUT_BIAS_of = 6  # Serializer output bias current

    # Register 10 (IO_SEU) - SEU test structure pad control register
    IO_SEU = 0x000A
    IO_SEU_RX_EN_of = 0  # Input receiver enable
    IO_SEU_RX_TERM_EN_of = 1  # Input termination enable
    IO_SEU_RX_BIAS_of = 2  # Receiver bias current
    IO_SEU_TX_BIAS_of = 6  # Serializer output bias current

    # Register 11 (IO_SET) - SET test structure pad control register
    IO_SET = 0x000B
    IO_SET_RX_EN_of = 0  # Input receiver enable
    IO_SET_RX_TERM_EN_of = 1  # Input termination enable
    IO_SET_RX_BIAS_of = 2  # Receiver bias current
    IO_SET_TX_BIAS_of = 6  # Serializer output bias current

    # Register 12 (PLL_SRO_CTRL0) - Single RO PLL Control Register 0
    PLL_SRO_CTRL0 = 0x000C
    PLL_SRO_CTRL0_DLF_NRST_of = 0  # Loop filter reset
    PLL_SRO_CTRL0_DLF_SDMNRST_of = 1  # SDM reset
    PLL_SRO_CTRL0_PD_SEL_of = 2  # Phase detector selection
    PLL_SRO_CTRL0_PD_SEL_DFQD1LVT = 0  # Standard DFQD1LVT
    PLL_SRO_CTRL0_PD_SEL_DFQD1LVT_NOCG = 1  # Improved DFQD1LVT
    PLL_SRO_CTRL0_PD_SEL_DFQD1LVT_COMB = 2  # NAND-Gate based DFF
    PLL_SRO_CTRL0_PD_SEL_DFQD1LVT_DUAL = 3  # Zero-hysteresis DFF
    PLL_SRO_CTRL0_MODE_SEL_of = 4  # Loop mode select
    PLL_SRO_CTRL0_MODE_SEL_PLL_NORM = 0  # PLL mode, direct PD DFF output to DLF
    PLL_SRO_CTRL0_MODE_SEL_PLL_NORM2 = 1  # PLL mode, same as above
    PLL_SRO_CTRL0_MODE_SEL_PLL_REGPD = 2  # PLL mode, registered PD DFF output to DLF
    PLL_SRO_CTRL0_MODE_SEL_PLL_REGPD2 = 3  # PLL mode, same as above
    PLL_SRO_CTRL0_MODE_SEL_CDR_NORM_EITHER = (
        4  # CDR mode, direct PD output to DLF, both data edges
    )
    PLL_SRO_CTRL0_MODE_SEL_CDR_NORM_RISING = (
        5  # CDR mode, direct PD output to DLF, rising data edges only
    )
    PLL_SRO_CTRL0_MODE_SEL_CDR_REGPD_EITHER = (
        6  # CDR mode, registered PD DFF output to DLF, both data edges
    )
    PLL_SRO_CTRL0_MODE_SEL_CDR_REGPD_RISING = (
        7  # CDR mode, registered PD DFF output to DLF, rising data edges only
    )

    # Register 13 (PLL_SRO_CTRL1) - Single RO PLL Control Register 1
    PLL_SRO_CTRL1 = 0x000D
    PLL_SRO_CTRL1_SER_PRBS_EN_of = 0  # Serializer PRBS enable
    PLL_SRO_CTRL1_SER_SCRAMBLER_BYPASS_of = 1  # Serializer scrambler bypass
    PLL_SRO_CTRL1_SER_EN_of = 2  # Serializer enable
    PLL_SRO_CTRL1_ACORR_LAG_of = 3  # Autocorrelation lag setting
    PLL_SRO_CTRL1_ACORR_START_of = 6  # Autocorrelation start command

    # Register 14 (PLL_SRO_CLKCTRL0) - Single RO PLL clock control register
    PLL_SRO_CLKCTRL0 = 0x000E
    PLL_SRO_CLKCTRL0_REF_FREQ_SEL_of = 0  # Loop reference clock / bit rate select
    PLL_SRO_CLKCTRL0_REF_FREQ_SEL_320M = 0  # 320 MHz Reference Clock
    PLL_SRO_CLKCTRL0_REF_FREQ_SEL_160M = 1  # 160 MHz Reference Clock
    PLL_SRO_CLKCTRL0_REF_FREQ_SEL_80M = 2  # 80 MHz Reference Clock
    PLL_SRO_CLKCTRL0_REF_FREQ_SEL_40M = 3  # 40 MHz Reference Clock
    PLL_SRO_CLKCTRL0_SDM_FREQ_SEL_of = 2  # Sigma Delta modulator frequency select
    PLL_SRO_CLKCTRL0_SDM_FREQ_SEL_640M = 0  # 640 MHz SDM frequency
    PLL_SRO_CLKCTRL0_SDM_FREQ_SEL_320M = 1  # 320 MHz SDM frequency
    PLL_SRO_CLKCTRL0_SDM_FREQ_SEL_160M = 2  # 160 MHz SDM frequency
    PLL_SRO_CLKCTRL0_SDM_FREQ_SEL_OFF = 3  # SDM gated
    PLL_SRO_CLKCTRL0_CLKOUT0_SEL_of = 4  # Clock output 0 frequency select
    PLL_SRO_CLKCTRL0_CLKOUT0_SEL_OFF = 0  # Clock output off
    PLL_SRO_CLKCTRL0_CLKOUT0_SEL_40M = 1  # Clock output frequency 40 MHz
    PLL_SRO_CLKCTRL0_CLKOUT0_SEL_80M = 2  # Clock output frequency 80 MHz
    PLL_SRO_CLKCTRL0_CLKOUT0_SEL_160M = 3  # Clock output frequency 160 MHz
    PLL_SRO_CLKCTRL0_CLKOUT0_SEL_320M = 4  # Clock output frequency 320 MHz
    PLL_SRO_CLKCTRL0_CLKOUT0_SEL_640M = 5  # Clock output frequency 640 MHz
    PLL_SRO_CLKCTRL0_CLKOUT0_SEL_1280M = 6  # Clock output frequency 1280 MHz
    PLL_SRO_CLKCTRL0_CLKOUT0_SEL_DOUT = 7  # CDR Data output

    # Register 15 (PLL_SRO_CLKCTRL1) - Single RO PLL clock control register
    PLL_SRO_CLKCTRL1 = 0x000F
    PLL_SRO_CLKCTRL1_CLKOUT1_SEL_of = 0  # Clock output 1 frequency select
    PLL_SRO_CLKCTRL1_CLKOUT1_SEL_OFF = 0  # Clock output off
    PLL_SRO_CLKCTRL1_CLKOUT1_SEL_40M = 1  # Clock output frequency 40 MHz
    PLL_SRO_CLKCTRL1_CLKOUT1_SEL_80M = 2  # Clock output frequency 80 MHz
    PLL_SRO_CLKCTRL1_CLKOUT1_SEL_160M = 3  # Clock output frequency 160 MHz
    PLL_SRO_CLKCTRL1_CLKOUT1_SEL_320M = 4  # Clock output frequency 320 MHz
    PLL_SRO_CLKCTRL1_CLKOUT1_SEL_640M = 5  # Clock output frequency 640 MHz
    PLL_SRO_CLKCTRL1_CLKOUT1_SEL_1280M = 6  # Clock output frequency 1280 MHz
    PLL_SRO_CLKCTRL1_CLKOUT1_SEL_DOUT = 7  # CDR Data output
    PLL_SRO_CLKCTRL1_CLKOUT2_SEL_of = 3  # Clock output 2 frequency select
    PLL_SRO_CLKCTRL1_CLKOUT2_SEL_OFF = 0  # Clock output off
    PLL_SRO_CLKCTRL1_CLKOUT2_SEL_40M = 1  # Clock output frequency 40 MHz
    PLL_SRO_CLKCTRL1_CLKOUT2_SEL_80M = 2  # Clock output frequency 80 MHz
    PLL_SRO_CLKCTRL1_CLKOUT2_SEL_160M = 3  # Clock output frequency 160 MHz
    PLL_SRO_CLKCTRL1_CLKOUT2_SEL_320M = 4  # Clock output frequency 320 MHz
    PLL_SRO_CLKCTRL1_CLKOUT2_SEL_640M = 5  # Clock output frequency 640 MHz
    PLL_SRO_CLKCTRL1_CLKOUT2_SEL_1280M = 6  # Clock output frequency 1280 MHz
    PLL_SRO_CLKCTRL1_CLKOUT2_SEL_DOUT = 7  # CDR Data output

    # Register 16 (PLL_SRO_DCO_CTRL) - Single RO DCO control register
    PLL_SRO_DCO_CTRL = 0x0010
    PLL_SRO_DCO_CTRL_DCO_EN_of = 0  # DCO enable
    PLL_SRO_DCO_CTRL_DCO_FTW_OFFS_of = 1  # DCO FTW offset

    # Register 17 (PLL_SRO_DLF_CONFIG0) - Single RO DLF configuration
    PLL_SRO_DLF_CONFIG0 = 0x0011
    PLL_SRO_DLF_CONFIG0_KP_of = 0  # Proportional loop gain
    PLL_SRO_DLF_CONFIG0_KI_of = 4  # Integral loop gain

    # Register 18 (PLL_SRO_DLF_CONFIG1) - Single RO DLF preset register
    PLL_SRO_DLF_CONFIG1 = 0x0012
    PLL_SRO_DLF_CONFIG1_INT_PRESETL_of = 0  # Integrator Preset Byte Low Byte

    # Register 19 (PLL_SRO_DLF_CONFIG2) - Single RO DLF preset register
    PLL_SRO_DLF_CONFIG2 = 0x0013
    PLL_SRO_DLF_CONFIG2_INT_PRESETH_of = 0  # Integrator Preset High Byte

    # Register 20 (PLL_SRO_ACQ_CTRL) - Single RO acquisition FSM control register
    PLL_SRO_ACQ_CTRL = 0x0014
    PLL_SRO_ACQ_CTRL_ACQ_EOC_REF_of = 0  # Reference clock end-of-count setting
    PLL_SRO_ACQ_CTRL_ACQ_EOC_DCO_of = 3  # DCO clock end-of-count setting
    PLL_SRO_ACQ_CTRL_ACQ_START_of = 6  # Acquisition FSM start command

    # Register 21 (PLL_TRO_CTRL0) - Hardened RO PLL Control Register 0
    PLL_TRO_CTRL0 = 0x0015
    PLL_TRO_CTRL0_DLF_NRST_of = 0  # Loop filter reset
    PLL_TRO_CTRL0_DLF_SDMNRST_of = 1  # SDM reset
    PLL_TRO_CTRL0_PD_SEL_of = 2  # Phase detector selection
    PLL_TRO_CTRL0_PD_SEL_DFQD1LVT = 0  # Standard DFQD1LVT
    PLL_TRO_CTRL0_PD_SEL_DFQD1LVT_NOCG = 1  # Improved DFQD1LVT
    PLL_TRO_CTRL0_PD_SEL_DFQD1LVT_COMB = 2  # NAND-Gate based DFF
    PLL_TRO_CTRL0_PD_SEL_DFQD1LVT_DUAL = 3  # Zero-hysteresis DFF
    PLL_TRO_CTRL0_MODE_SEL_of = 4  # Loop mode select
    PLL_TRO_CTRL0_MODE_SEL_PLL_NORM = 0  # PLL mode, direct PD DFF output to DLF
    PLL_TRO_CTRL0_MODE_SEL_PLL_NORM2 = 1  # PLL mode, same as above
    PLL_TRO_CTRL0_MODE_SEL_PLL_REGPD = 2  # PLL mode, registered PD DFF output to DLF
    PLL_TRO_CTRL0_MODE_SEL_PLL_REGPD2 = 3  # PLL mode, same as above
    PLL_TRO_CTRL0_MODE_SEL_CDR_NORM_EITHER = (
        4  # CDR mode, direct PD output to DLF, both data edges
    )
    PLL_TRO_CTRL0_MODE_SEL_CDR_NORM_RISING = (
        5  # CDR mode, direct PD output to DLF, rising data edges only
    )
    PLL_TRO_CTRL0_MODE_SEL_CDR_REGPD_EITHER = (
        6  # CDR mode, registered PD DFF output to DLF, both data edges
    )
    PLL_TRO_CTRL0_MODE_SEL_CDR_REGPD_RISING = (
        7  # CDR mode, registered PD DFF output to DLF, rising data edges only
    )

    # Register 22 (PLL_TRO_CTRL1) - Single RO PLL Control Register 1
    PLL_TRO_CTRL1 = 0x0016
    PLL_TRO_CTRL1_SER_PRBS_EN_of = 0  # Serializer PRBS enable
    PLL_TRO_CTRL1_SER_SCRAMBLER_BYPASS_of = 1  # Serializer scrambler bypass
    PLL_TRO_CTRL1_SER_EN_of = 2  # Serializer enable
    PLL_TRO_CTRL1_ACORR_LAG_of = 3  # Autocorrelation lag setting
    PLL_TRO_CTRL1_ACORR_START_of = 6  # Autocorrelation start command

    # Register 23 (PLL_TRO_CLKCTRL0) - Single RO PLL clock control register
    PLL_TRO_CLKCTRL0 = 0x0017
    PLL_TRO_CLKCTRL0_REF_FREQ_SEL_of = 0  # Loop reference clock / bit rate select
    PLL_TRO_CLKCTRL0_REF_FREQ_SEL_320M = 0  # 320 MHz Reference Clock
    PLL_TRO_CLKCTRL0_REF_FREQ_SEL_160M = 1  # 160 MHz Reference Clock
    PLL_TRO_CLKCTRL0_REF_FREQ_SEL_80M = 2  # 80 MHz Reference Clock
    PLL_TRO_CLKCTRL0_REF_FREQ_SEL_40M = 3  # 40 MHz Reference Clock
    PLL_TRO_CLKCTRL0_SDM_FREQ_SEL_of = 2  # Sigma Delta modulator frequency select
    PLL_TRO_CLKCTRL0_SDM_FREQ_SEL_640M = 0  # 640 MHz SDM frequency
    PLL_TRO_CLKCTRL0_SDM_FREQ_SEL_320M = 1  # 320 MHz SDM frequency
    PLL_TRO_CLKCTRL0_SDM_FREQ_SEL_160M = 2  # 160 MHz SDM frequency
    PLL_TRO_CLKCTRL0_SDM_FREQ_SEL_OFF = 3  # SDM gated
    PLL_TRO_CLKCTRL0_CLKOUT0_SEL_of = 4  # Clock output 0 frequency select
    PLL_TRO_CLKCTRL0_CLKOUT0_SEL_OFF = 0  # Clock output off
    PLL_TRO_CLKCTRL0_CLKOUT0_SEL_40M = 1  # Clock output frequency 40 MHz
    PLL_TRO_CLKCTRL0_CLKOUT0_SEL_80M = 2  # Clock output frequency 80 MHz
    PLL_TRO_CLKCTRL0_CLKOUT0_SEL_160M = 3  # Clock output frequency 160 MHz
    PLL_TRO_CLKCTRL0_CLKOUT0_SEL_320M = 4  # Clock output frequency 320 MHz
    PLL_TRO_CLKCTRL0_CLKOUT0_SEL_640M = 5  # Clock output frequency 640 MHz
    PLL_TRO_CLKCTRL0_CLKOUT0_SEL_1280M = 6  # Clock output frequency 1280 MHz
    PLL_TRO_CLKCTRL0_CLKOUT0_SEL_DOUT = 7  # CDR Data output

    # Register 24 (PLL_TRO_CLKCTRL1) - Single RO PLL clock control register
    PLL_TRO_CLKCTRL1 = 0x0018
    PLL_TRO_CLKCTRL1_CLKOUT1_SEL_of = 0  # Clock output 1 frequency select
    PLL_TRO_CLKCTRL1_CLKOUT1_SEL_OFF = 0  # Clock output off
    PLL_TRO_CLKCTRL1_CLKOUT1_SEL_40M = 1  # Clock output frequency 40 MHz
    PLL_TRO_CLKCTRL1_CLKOUT1_SEL_80M = 2  # Clock output frequency 80 MHz
    PLL_TRO_CLKCTRL1_CLKOUT1_SEL_160M = 3  # Clock output frequency 160 MHz
    PLL_TRO_CLKCTRL1_CLKOUT1_SEL_320M = 4  # Clock output frequency 320 MHz
    PLL_TRO_CLKCTRL1_CLKOUT1_SEL_640M = 5  # Clock output frequency 640 MHz
    PLL_TRO_CLKCTRL1_CLKOUT1_SEL_1280M = 6  # Clock output frequency 1280 MHz
    PLL_TRO_CLKCTRL1_CLKOUT1_SEL_DOUT = 7  # CDR Data output
    PLL_TRO_CLKCTRL1_CLKOUT2_SEL_of = 3  # Clock output 2 frequency select
    PLL_TRO_CLKCTRL1_CLKOUT2_SEL_OFF = 0  # Clock output off
    PLL_TRO_CLKCTRL1_CLKOUT2_SEL_40M = 1  # Clock output frequency 40 MHz
    PLL_TRO_CLKCTRL1_CLKOUT2_SEL_80M = 2  # Clock output frequency 80 MHz
    PLL_TRO_CLKCTRL1_CLKOUT2_SEL_160M = 3  # Clock output frequency 160 MHz
    PLL_TRO_CLKCTRL1_CLKOUT2_SEL_320M = 4  # Clock output frequency 320 MHz
    PLL_TRO_CLKCTRL1_CLKOUT2_SEL_640M = 5  # Clock output frequency 640 MHz
    PLL_TRO_CLKCTRL1_CLKOUT2_SEL_1280M = 6  # Clock output frequency 1280 MHz
    PLL_TRO_CLKCTRL1_CLKOUT2_SEL_DOUT = 7  # CDR Data output

    # Register 25 (PLL_TRO_DCO_CTRL) - Single RO DCO control register
    PLL_TRO_DCO_CTRL = 0x0019
    PLL_TRO_DCO_CTRL_DCO_EN_of = 0  # DCO enable
    PLL_TRO_DCO_CTRL_DCO_FTW_OFFS_of = 1  # DCO FTW offset

    # Register 26 (PLL_TRO_DLF_CONFIG0) - Single RO DLF configuration
    PLL_TRO_DLF_CONFIG0 = 0x001A
    PLL_TRO_DLF_CONFIG0_KP_of = 0  # Proportional loop gain
    PLL_TRO_DLF_CONFIG0_KI_of = 4  # Integral loop gain

    # Register 27 (PLL_TRO_DLF_CONFIG1) - Single RO DLF preset register
    PLL_TRO_DLF_CONFIG1 = 0x001B
    PLL_TRO_DLF_CONFIG1_INT_PRESETL_of = 0  # Integrator Preset Byte Low Byte

    # Register 28 (PLL_TRO_DLF_CONFIG2) - Single RO DLF preset register
    PLL_TRO_DLF_CONFIG2 = 0x001C
    PLL_TRO_DLF_CONFIG2_INT_PRESETH_of = 0  # Integrator Preset High Byte

    # Register 29 (PLL_TRO_ACQ_CTRL) - Single RO acquisition FSM control register
    PLL_TRO_ACQ_CTRL = 0x001D
    PLL_TRO_ACQ_CTRL_ACQ_EOC_REF_of = 0  # Reference clock end-of-count setting
    PLL_TRO_ACQ_CTRL_ACQ_EOC_DCO_of = 3  # DCO clock end-of-count setting
    PLL_TRO_ACQ_CTRL_ACQ_START_of = 6  # Acquisition FSM start command

    # Register 30 (PLL_LC_CTRL0) - LC PLL Control Register 0
    PLL_LC_CTRL0 = 0x001E
    PLL_LC_CTRL0_DLF_NRST_of = 0  # Loop filter reset
    PLL_LC_CTRL0_DLF_SDMNRST_of = 1  # SDM reset
    PLL_LC_CTRL0_PD_SEL_of = 2  # Phase detector selection
    PLL_LC_CTRL0_PD_SEL_DFQD1LVT = 0  # Standard DFQD1LVT
    PLL_LC_CTRL0_PD_SEL_DFQD1LVT_NOCG = 1  # Improved DFQD1LVT
    PLL_LC_CTRL0_PD_SEL_DFQD1LVT_COMB = 2  # NAND-Gate based DFF
    PLL_LC_CTRL0_PD_SEL_DFQD1LVT_DUAL = 3  # Zero-hysteresis DFF
    PLL_LC_CTRL0_MODE_SEL_of = 4  # Loop mode select
    PLL_LC_CTRL0_MODE_SEL_PLL_NORM = 0  # PLL mode, direct PD DFF output to DLF
    PLL_LC_CTRL0_MODE_SEL_PLL_NORM2 = 1  # PLL mode, same as above
    PLL_LC_CTRL0_MODE_SEL_PLL_REGPD = 2  # PLL mode, registered PD DFF output to DLF
    PLL_LC_CTRL0_MODE_SEL_PLL_REGPD2 = 3  # PLL mode, same as above
    PLL_LC_CTRL0_MODE_SEL_CDR_NORM_EITHER = (
        4  # CDR mode, direct PD output to DLF, both data edges
    )
    PLL_LC_CTRL0_MODE_SEL_CDR_NORM_RISING = (
        5  # CDR mode, direct PD output to DLF, rising data edges only
    )
    PLL_LC_CTRL0_MODE_SEL_CDR_REGPD_EITHER = (
        6  # CDR mode, registered PD DFF output to DLF, both data edges
    )
    PLL_LC_CTRL0_MODE_SEL_CDR_REGPD_RISING = (
        7  # CDR mode, registered PD DFF output to DLF, rising data edges only
    )

    # Register 31 (PLL_LC_CTRL1) - LC PLL Control Register 1
    PLL_LC_CTRL1 = 0x001F
    PLL_LC_CTRL1_SER_PRBS_EN_of = 0  # Serializer PRBS enable
    PLL_LC_CTRL1_SER_SCRAMBLER_BYPASS_of = 1  # Serializer scrambler bypass
    PLL_LC_CTRL1_SER_EN_of = 2  # Serializer enable
    PLL_LC_CTRL1_ACORR_LAG_of = 3  # Autocorrelation lag setting
    PLL_LC_CTRL1_ACORR_START_of = 6  # Autocorrelation start command

    # Register 32 (PLL_LC_CLKCTRL0) - LC PLL clock control register
    PLL_LC_CLKCTRL0 = 0x0020
    PLL_LC_CLKCTRL0_REF_FREQ_SEL_of = 0  # Loop reference clock / bit rate select
    PLL_LC_CLKCTRL0_REF_FREQ_SEL_320M = 0  # 320 MHz Reference Clock
    PLL_LC_CLKCTRL0_REF_FREQ_SEL_160M = 1  # 160 MHz Reference Clock
    PLL_LC_CLKCTRL0_REF_FREQ_SEL_80M = 2  # 80 MHz Reference Clock
    PLL_LC_CLKCTRL0_REF_FREQ_SEL_40M = 3  # 40 MHz Reference Clock
    PLL_LC_CLKCTRL0_SDM_FREQ_SEL_of = 2  # Sigma Delta modulator frequency select
    PLL_LC_CLKCTRL0_SDM_FREQ_SEL_640M = 0  # 640 MHz SDM frequency
    PLL_LC_CLKCTRL0_SDM_FREQ_SEL_320M = 1  # 320 MHz SDM frequency
    PLL_LC_CLKCTRL0_SDM_FREQ_SEL_160M = 2  # 160 MHz SDM frequency
    PLL_LC_CLKCTRL0_SDM_FREQ_SEL_OFF = 3  # SDM gated
    PLL_LC_CLKCTRL0_CLKOUT0_SEL_of = 4  # Clock output 0 frequency select
    PLL_LC_CLKCTRL0_CLKOUT0_SEL_OFF = 0  # Clock output off
    PLL_LC_CLKCTRL0_CLKOUT0_SEL_40M = 1  # Clock output frequency 40 MHz
    PLL_LC_CLKCTRL0_CLKOUT0_SEL_80M = 2  # Clock output frequency 80 MHz
    PLL_LC_CLKCTRL0_CLKOUT0_SEL_160M = 3  # Clock output frequency 160 MHz
    PLL_LC_CLKCTRL0_CLKOUT0_SEL_320M = 4  # Clock output frequency 320 MHz
    PLL_LC_CLKCTRL0_CLKOUT0_SEL_640M = 5  # Clock output frequency 640 MHz
    PLL_LC_CLKCTRL0_CLKOUT0_SEL_1280M = 6  # Clock output frequency 1280 MHz
    PLL_LC_CLKCTRL0_CLKOUT0_SEL_DOUT = 7  # CDR Data output

    # Register 33 (PLL_LC_CLKCTRL1) - LC PLL clock control register
    PLL_LC_CLKCTRL1 = 0x0021
    PLL_LC_CLKCTRL1_CLKOUT1_SEL_of = 0  # Clock output 1 frequency select
    PLL_LC_CLKCTRL1_CLKOUT1_SEL_OFF = 0  # Clock output off
    PLL_LC_CLKCTRL1_CLKOUT1_SEL_40M = 1  # Clock output frequency 40 MHz
    PLL_LC_CLKCTRL1_CLKOUT1_SEL_80M = 2  # Clock output frequency 80 MHz
    PLL_LC_CLKCTRL1_CLKOUT1_SEL_160M = 3  # Clock output frequency 160 MHz
    PLL_LC_CLKCTRL1_CLKOUT1_SEL_320M = 4  # Clock output frequency 320 MHz
    PLL_LC_CLKCTRL1_CLKOUT1_SEL_640M = 5  # Clock output frequency 640 MHz
    PLL_LC_CLKCTRL1_CLKOUT1_SEL_1280M = 6  # Clock output frequency 1280 MHz
    PLL_LC_CLKCTRL1_CLKOUT1_SEL_DOUT = 7  # CDR Data output
    PLL_LC_CLKCTRL1_CLKOUT2_SEL_of = 3  # Clock output 2 frequency select
    PLL_LC_CLKCTRL1_CLKOUT2_SEL_OFF = 0  # Clock output off
    PLL_LC_CLKCTRL1_CLKOUT2_SEL_40M = 1  # Clock output frequency 40 MHz
    PLL_LC_CLKCTRL1_CLKOUT2_SEL_80M = 2  # Clock output frequency 80 MHz
    PLL_LC_CLKCTRL1_CLKOUT2_SEL_160M = 3  # Clock output frequency 160 MHz
    PLL_LC_CLKCTRL1_CLKOUT2_SEL_320M = 4  # Clock output frequency 320 MHz
    PLL_LC_CLKCTRL1_CLKOUT2_SEL_640M = 5  # Clock output frequency 640 MHz
    PLL_LC_CLKCTRL1_CLKOUT2_SEL_1280M = 6  # Clock output frequency 1280 MHz
    PLL_LC_CLKCTRL1_CLKOUT2_SEL_DOUT = 7  # CDR Data output

    # Register 34 (PLL_LC_DCO_CTRL0) - LC DCO control register 0
    PLL_LC_DCO_CTRL0 = 0x0022
    PLL_LC_DCO_CTRL0_IBIAS_BYPASS_of = 0  # Bias generator bypass
    PLL_LC_DCO_CTRL0_IBIAS_of = 1  # Bias generator current

    # Register 35 (PLL_LC_DCO_CTRL1) - LC DCO control register 1
    PLL_LC_DCO_CTRL1 = 0x0023
    PLL_LC_DCO_CTRL1_VBIAS_PVT_ACQ_of = 0  # PVT/ACQ bias generator current

    # Register 36 (PLL_LC_DCO_CTRL2) - LC DCO control register 2
    PLL_LC_DCO_CTRL2 = 0x0024
    PLL_LC_DCO_CTRL2_VBIAS_TRK_of = 0  # TRK bias generator current

    # Register 37 (PLL_LC_DCO_CTRL3) - LC DCO control register 3
    PLL_LC_DCO_CTRL3 = 0x0025
    PLL_LC_DCO_CTRL3_PVT_of = 0  # PVT bank setting

    # Register 38 (PLL_LC_DCO_CTRL4) - LC DCO control register 4
    PLL_LC_DCO_CTRL4 = 0x0026
    PLL_LC_DCO_CTRL4_ACQ_of = 0  # Acquisition bank setting

    # Register 39 (PLL_LC_DLF_CONFIG0) - LC DLF configuration
    PLL_LC_DLF_CONFIG0 = 0x0027
    PLL_LC_DLF_CONFIG0_KP_of = 0  # Proportional loop gain
    PLL_LC_DLF_CONFIG0_KI_of = 4  # Integral loop gain

    # Register 40 (PLL_LC_DLF_CONFIG1) - LC DLF preset register
    PLL_LC_DLF_CONFIG1 = 0x0028
    PLL_LC_DLF_CONFIG1_INT_PRESETL_of = 0  # Integrator Preset Byte Low Byte

    # Register 41 (PLL_LC_DLF_CONFIG2) - LC DLF preset register
    PLL_LC_DLF_CONFIG2 = 0x0029
    PLL_LC_DLF_CONFIG2_INT_PRESETH_of = 0  # Integrator Preset High Byte

    # Register 42 (PLL_LC_ACQ_CTRL) - LC acquisition FSM control register
    PLL_LC_ACQ_CTRL = 0x002A
    PLL_LC_ACQ_CTRL_ACQ_EOC_REF_of = 0  # Reference clock end-of-count setting
    PLL_LC_ACQ_CTRL_ACQ_EOC_DCO_of = 3  # DCO clock end-of-count setting
    PLL_LC_ACQ_CTRL_ACQ_START_of = 6  # Acquisition FSM start command

    # Register 43 (SEUCNT0) - SEU counter
    SEUCNT0 = 0x002B
    SEUCNT0_SEUCNT0_of = 0  # Bits 7 down to 0 fof SEU counter

    # Register 44 (SEUCNT1) - SEU counter
    SEUCNT1 = 0x002C
    SEUCNT1_SEUCNT1_of = 0  # Bits 15 down to 8 fof SEU counter

    # Register 45 (SEUCNT2) - SEU counter
    SEUCNT2 = 0x002D
    SEUCNT2_SEUCNT2_of = 0  # Bits 23 down to 16 fof SEU counter

    # Register 46 (SEUCNT3) - SEU counter
    SEUCNT3 = 0x002E
    SEUCNT3_SEUCNT3_of = 0  # Bits 31 down to 24 fof SEU counter

    # Register 47 (PLL_SRO_FSM_RESULT) - SRO PLL State Machine results
    PLL_SRO_FSM_RESULT = 0x002F
    PLL_SRO_FSM_RESULT_ACQ_DONE_of = 0  # Acquisition FSM done for SRO PLL
    PLL_SRO_FSM_RESULT_ACQ_DCO_FAST_of = 1  # Acquisition FSM result for SRO PLL
    PLL_SRO_FSM_RESULT_ACORR_DONE_of = 2  # Autocorrelation measurement done for SRO PLL
    PLL_SRO_FSM_RESULT_ACORR_SIGN_of = 3  # Autocorrelation sign done for SRO PLL
    PLL_SRO_FSM_RESULT_ACORR_MAG_of = 4  # Autocorrelation magnitude done for SRO PLL

    # Register 48 (PLL_TRO_FSM_RESULT) - TRO PLL State Machine results
    PLL_TRO_FSM_RESULT = 0x0030
    PLL_TRO_FSM_RESULT_ACQ_DONE_of = 0  # Acquisition FSM done for TRO PLL
    PLL_TRO_FSM_RESULT_ACQ_DCO_FAST_of = 1  # Acquisition FSM result for TRO PLL
    PLL_TRO_FSM_RESULT_ACORR_DONE_of = 2  # Autocorrelation measurement done for TRO PLL
    PLL_TRO_FSM_RESULT_ACORR_SIGN_of = 3  # Autocorrelation sign done for TRO PLL
    PLL_TRO_FSM_RESULT_ACORR_MAG_of = 4  # Autocorrelation magnitude done for TRO PLL

    # Register 49 (PLL_LC_FSM_RESULT) - LC PLL State Machine results
    PLL_LC_FSM_RESULT = 0x0031
    PLL_LC_FSM_RESULT_ACQ_DONE_of = 0  # Acquisition FSM done for LC PLL
    PLL_LC_FSM_RESULT_ACQ_DCO_FAST_of = 1  # Acquisition FSM result for LC PLL
    PLL_LC_FSM_RESULT_ACORR_DONE_of = 2  # Autocorrelation measurement done for LC PLL
    PLL_LC_FSM_RESULT_ACORR_SIGN_of = 3  # Autocorrelation sign done for LC PLL
    PLL_LC_FSM_RESULT_ACORR_MAG_of = 4  # Autocorrelation magnitude done for LC PLL
