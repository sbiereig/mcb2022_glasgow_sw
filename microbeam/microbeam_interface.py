import asyncio
import collections

class MicrobeamInterface:
    def __init__(self, iface, applet, device, logger):
        self._lower     = iface
        self._applet    = applet
        self._device    = device
        self._logger    = logger
        self._i2c_shadow_regs = collections.defaultdict(int)

    async def set_led(self, num, value):
        reg = await self._device.read_register(self._applet.reg_addrs["led"])
        if value:
            reg |= (1 << num)
        else:
            reg &= ~(1 << num)
        await self._device.write_register(self._applet.reg_addrs["led"], reg)

    async def write_dac(self, x, y):
        """Write new X/Y position to DAC"""
        await self._lower.write([x & 0xff, (x >> 8) & 0xff, y & 0xff, (y >> 8) & 0xff])
        await self._lower.flush()

    async def read_hit(self):
        """Wait for and read a new hit (positive edge on TRIGGER input).
        Returned timestamp is in milliseconds after applet start and position is in DAC LSBs (16 bit signed)
        """
        data = await self._lower.read(length=8, flush=False)
        timestamp = data[0] + (data[1] << 8) + (data[2] << 16) + (data[3] << 24)
        x = data[4] + (data[5] << 8)
        y = data[6] + (data[7] << 8)
        if x > 32767: x -= 65536
        if y > 32767: y -= 65536
        return timestamp, x, y

    async def write_spi(self, data):
        """Write full config into ljPLL (expects 92 bit config)"""
        await self._device.write_register(self._applet.reg_addrs["spi_data"], data, width=96//8)
        await self._device.write_register(self._applet.reg_addrs["spi_launch"], 1)
        await self._device.write_register(self._applet.reg_addrs["spi_launch"], 0)

    async def write_i2c(self, slave_addr, reg_addr, reg_data):
        """Write one byte of data via I2C?"""
        await self._device.write_register(self._applet.reg_addrs["i2c_slave_addr"], slave_addr)
        await self._device.write_register(self._applet.reg_addrs["i2c_reg_addr"], reg_addr, width=16//8)
        await self._device.write_register(self._applet.reg_addrs["i2c_reg_data"], reg_data)
        await self._device.write_register(self._applet.reg_addrs["i2c_launch"], 1)
        await self._device.write_register(self._applet.reg_addrs["i2c_launch"], 0)
        self._i2c_shadow_regs[reg_addr] = reg_data
        return await self._device.read_register(self._applet.reg_addrs["i2c_ack"])

    def read_i2c(self, slave_addr, reg_addr):
        """Single-byte emulated I2C read"""
        return self._i2c_shadow_regs[reg_addr]

    async def write_i2c_dart(self, reg_addr, reg_data):
        for idx, d in enumerate(reg_data):
            await self.write_i2c(0x20, reg_addr+idx, reg_data[idx])

    async def read_i2c_dart(self, reg_addr, read_len):
        self._logger.warn("Performed a read using shadow registers only!")
        result = []
        for i in read_len:
            result.append(self.read_i2c(0x20, reg_addr + i))
        return result

    async def set_shutter_time_ms(self, time):
        """Shutter time in milliseconds - will be rounded to 10µs precision"""
        assert time < 650, "Invalid shutter time provided"
        time_10us = int(time * 100)
        await self._device.write_register(self._applet.reg_addrs["shutter_time"], time_10us, width=16//8)

    async def set_shutter_override(self, enable):
        """Permanently activates the shutter"""
        await self._device.write_register(self._applet.reg_addrs["shutter_override"], enable)

    async def asic_reset(self):
        """Consistently use active-low reset convention here. Inversion for ljPLL is done in hardware."""
        await self._device.write_register(self._applet.reg_addrs["asic_reset"], 0)
        await asyncio.sleep(0.01)
        await self._device.write_register(self._applet.reg_addrs["asic_reset"], 1)

