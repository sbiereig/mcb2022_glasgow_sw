import logging
import asyncio
import os

import numpy as np
import dart28_drivers

from .microbeam_subtarget import MicrobeamSubtarget
from .microbeam_interface import MicrobeamInterface
from .microbeam_web import MicrobeamWebInterface
from .microbeam_run_controller import MicrobeamRunController
from .drivers.adpll import Adpll
from .drivers.ljpll import Ljpll
from .drivers.lcdco import LcDco
from ... import *

class MicrobeamApplet(GlasgowApplet):
    logger = logging.getLogger(__name__)
    help = "Microbeam control applet"
    description = ""

    __pins = ("sda", "scl", "asic_reset", "asic_scapt", "sr_dout", "sr_shld", "sr_clk", "mcb_shutter", "mcb_trigger", "dac_ldac", "dac_sdin", "dac_sclk", "dac_sync")
    __pin_defaults = (0, 1, 2, 3, 5, 6, 7, 8, 9, 12, 13, 14, 15)

    @classmethod
    def add_build_arguments(cls, parser, access):
        super().add_build_arguments(parser, access)

        parser.add_argument("-d", "--dut", metavar="DUT", choices=["adpll", "ljpll", "dart28", "lcdco"], required=True, help="Type of DUT")
        parser.add_argument("-b", "--beam-interface", metavar="BEAM_IF", choices=["write", "read"], required=True, help="Type of beam position interface")

        for pin, default in zip(cls.__pins, cls.__pin_defaults):
            access.add_pin_argument(parser, pin, default=default)

    def build(self, target, args):
        self.mux_interface = iface = target.multiplexer.claim_interface(self, args)
        self.reg_addrs = {}

        regs = {}
        regs["led"], self.reg_addrs["led"] = target.registers.add_rw(5)
        regs["asic_reset"], self.reg_addrs["asic_reset"] = target.registers.add_rw(1, reset=1)
        regs["spi_data"], self.reg_addrs["spi_data"] = target.registers.add_rw(96)
        regs["spi_launch"], self.reg_addrs["spi_launch"] = target.registers.add_rw(1)
        regs["i2c_slave_addr"], self.reg_addrs["i2c_slave_addr"] = target.registers.add_rw(8)
        regs["i2c_reg_addr"], self.reg_addrs["i2c_reg_addr"] = target.registers.add_rw(16)
        regs["i2c_reg_data"], self.reg_addrs["i2c_reg_data"] = target.registers.add_rw(8)
        regs["i2c_launch"], self.reg_addrs["i2c_launch"] = target.registers.add_rw(1)
        regs["i2c_ack"], self.reg_addrs["i2c_ack"] = target.registers.add_ro(1)
        regs["shutter_time"], self.reg_addrs["shutter_time"] = target.registers.add_rw(16)
        regs["shutter_override"], self.reg_addrs["shutter_override"] = target.registers.add_rw(1, reset=1)

        leds = [target.platform.request("led", n) for n in range(5)]

        pads = iface.get_pads(args, pins=self.__pins)

        iface.add_subtarget(MicrobeamSubtarget(
            dut=args.dut,
            beam_if=args.beam_interface,
            regs=regs,
            leds=leds,
            pads=pads,
            in_fifo=iface.get_in_fifo(),
            out_fifo=iface.get_out_fifo(),
        ))

    @classmethod
    def add_run_arguments(cls, parser, access):
        pass

    async def run(self, device, args):
        # start logging to file
        log_formatter = logging.Formatter("%(asctime)s [%(levelname)-5.5s]  %(message)s")
        log_file_handler = logging.FileHandler(os.path.join(os.getcwd(), "glasgow_log.txt"))
        log_file_handler.setFormatter(log_formatter)
        logging.getLogger().addHandler(log_file_handler)
        self.logger.info("Glasgow microbeam control startup")

        if args.dut == "dart28":
            await device.set_voltage("A", 1.8)
        else:
            await device.set_voltage("A", 3.3)
        await device.set_voltage("B", 5.0)
        self.logger.info("I/O voltage setup complete")
        iface = await device.demultiplexer.claim_interface(self, self.mux_interface, args)
        return MicrobeamInterface(iface=iface, applet=self, device=device, logger=self.logger)

    @classmethod
    def add_interact_arguments(cls, parser):
        pass

    async def interact(self, device, args, iface):
        self.logger.info("Performing DUT reset + configuration")
        await iface.asic_reset()

        if args.dut == "adpll":
            adpll = Adpll(
                i2c_write=iface.write_i2c,
                logger=self.logger,
                chip_address=0x40
            )

            await adpll.setup_io_default("LC")
            await adpll.setup_pll_basic("LC")
            await adpll.setup_pll_clock("LC", "40M", "640M")
            await adpll.setup_pll_clkout("LC", 2, "1280M")
            await adpll.setup_pll_dlf_preset("LC", int(2 ** 15))
            await adpll.setup_pll_dco(
                "LC",
                {
                    "ibias": 8,
                    "ibias_bypass": True,
                    "vbias_pvt_acq": 8,
                    "vbias_trk": 8,
                    "pvt": 20,
                    "acq": 47,
                },
            )

        if args.dut == "ljpll":
            ljpll = Ljpll(
                spi_write=iface.write_spi
            )
            ljpll.set_global_irx(enable=True)
            ljpll.set_global_abuff(enable=False)
            ljpll.set_global_obuff(enable=True, dac=31)
            ljpll.set_global_buffen(buff0=False, buff1=False, buff2=True, buff3=False)
            ljpll.set_lc_pll(pd_enable=False, cp_enable=False)
            ljpll.set_lc_vco(
                bias_en=True,
                igen_start=True,
                dac=32,
                railmode=True,
                capsel=2,
                vtune_ext=True,
                afc=False
            )
            await ljpll.program()

        if args.dut == "dart28":
            dart28 = dart28_drivers.Dart28NrzDriver(
                write_regs=iface.write_i2c_dart,
                read_regs=iface.read_i2c_dart,
            )
            channel = 1
            # disable / power-down all lanes
            for i in range(4):
                # disable data generation, set to constant-zero pattern
                await dart28.data_gen[i].config(
                    data_pattern=dart28_drivers.DataGeneratorPattern.RAW_CONSTANT
                )
                # bypass (clock-gate) data path, set to constant-zero pattern
                await dart28.hst[i].data_path.dft_config(
                    data_path_bypass=True, data_pattern=dart28_drivers.DftDataPattern.RAW_CONSTANT
                )
                # disable all serializers
                await dart28.hst[i].high_speed_output.config_serializer(
                    en_a=False, en_b=False, en_c=False, en_preemph=False
                )
                # mute output driver
                await dart28.hst[i].high_speed_output.config_driver_main(
                    mode_p=dart28_drivers.DriverMode.GND,
                    mode_n=dart28_drivers.DriverMode.GND,
                )
                await dart28.hst[i].high_speed_output.config_driver_pre(
                    mode_p=dart28_drivers.DriverMode.GND,
                    mode_n=dart28_drivers.DriverMode.GND,
                )
                await dart28.hst[i].high_speed_output.config_driver_post(
                    mode_p=dart28_drivers.DriverMode.GND,
                    mode_n=dart28_drivers.DriverMode.GND,
                )
                # place PLL in reset
                await dart28.hst[i].pll.config_pll(
                    soft_rst=True,
                )
                # cut off DCO bias current
                await dart28.hst[i].pll.config_dco(gm_bias=0xFF)
            # configure PLL of enabled channel
            await dart28.hst[channel].pll.config_dco(gm_bias=0x1F)
            await dart28.hst[channel].pll.config_tdc(
                offs_ref=0, offs_fb=0, thresh_see=4, thresh_ld=4
            )
            await dart28.hst[channel].pll.config_dlf(
                k_wide={
                    "kp_bbpd": 15,
                    "kp_tdc": 3,
                    "ki_bbpd": 8,
                    "ki_tdc": 5,
                    "ki_fd": 8,
                },
                k_narrow={
                    "kp_bbpd": 1,
                    "kp_tdc": 0,
                    "ki_bbpd": 2,
                    "ki_tdc": 1,
                    "ki_fd": 0,
                },
                prop_gate_pattern=0xFC,
            )

            await dart28.hst[channel].pll.config_pll(soft_rst=True)
            await dart28.hst[channel].pll.config_pll(soft_rst=False)
            test_output_sel = [
                dart28_drivers.IoRingToselect.HST0_TO0,
                dart28_drivers.IoRingToselect.HST1_TO0,
                dart28_drivers.IoRingToselect.HST2_TO0,
                dart28_drivers.IoRingToselect.HST3_TO0,
            ][channel]
            await dart28.io_ring.config_test_output(
                2,
                test_output_sel,
                drive_strength=15,
                cmm=0,
            )

            await dart28.hst[channel].test_output_mux.config_test_output(
                0, dart28_drivers.Toselect.CLK_X16A
            )

            await dart28.hst[channel].pll.dft_override_dco_pvt_acq(pvt=0x01, acq=27)
            await dart28.hst[channel].pll.dft_override_dlf(rst_dlf=True, rst_sdm=True, int_val=40 << 10)

        if args.dut == "lcdco":
            lcdco = LcDco(
                write_spi=iface.write_spi
            )
            await lcdco.config_dco(
                num=0,
                bias_gm=7,
                bias_bypass=False,
                bank_pvt=32,
                bank_acq=40,
                bank_trk=32,
                mux_sel_div=LcDco.MUX_SEL_DIV_1280M,
            )


        shutter_time_ms = 10
        self.logger.info(f"Configuring shutter time of {shutter_time_ms} ms")
        await iface.set_shutter_time_ms(shutter_time_ms)
        await iface.set_shutter_override(True)

        run_ctrl = MicrobeamRunController(self.logger, iface)
        await run_ctrl.start()

        web_if = MicrobeamWebInterface(self.logger, run_ctrl)
        await web_if.serve()

class MicrobeamAppletTestCase(GlasgowAppletTestCase, applet=MicrobeamApplet):
    @synthesis_test
    def test_build(self):
        self.assertBuilds()
