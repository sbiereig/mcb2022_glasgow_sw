# Microbeam Experiment Controller Software/Firmware

## Installation
Check out this repository to within a glasgow applet source tree, and add an appropriate import to `software/glasgow/applet/all.py`. Unfortunately, out-of-tree applets are not supported at the moment.

## Operation
The system software is launched using the glasgow exectutable. Parameters for the DUT type (`-d adpll` or `-d ljpll`) and the beam control mode (`-b write` or `-b read`) need to be provided. Example:

```
glasgow run microbeam -d adpll -b write
```

All data is written to the current working directory (i.e. launch this from an appropriate `run` directory). When running, the software offers two primary types of interaction:
  * Web control interface (on port 8088)
  * Subscriber TCP interface (on port 8188)


## Calibration
The system may be calibrated to directly enter scan coordinate/windows in micrometers, instead of volts or DAC LSBs.
Calibration should be performed using a scan of a heavy ion target (e.g. hex-grid) with well-characterized features. A scaling coefficient for the X and Y direction of the beam may be provided in a file called `cal.json` in the current working directory.
This file should contain the following format:

```
{
    "lsb_per_um_x": 1000.0,
    "lsb_per_um_y": 500.0
}
```

Where the calibration is specified in terms of DAC LSBs per micrometer, which can be given as a floating point number for the needed precision.

## Subscriber TCP interface
To control external software, the software exposes a TCP socket supplying the following information using a line-based text protocol:
  * `start_run XXX`, where XXX is the number of the new run
  * `stop_run`, indicating the completion of a run
  * `pos XXX YYY`, where XXX and YYY are the X and Y coordinates, given as signed DAC codes (integer, -32768..32767 inclusive)

An example scan looks like:

```
start_run 127
pos -3277 -3277
pos 3277 -3277
pos -3277 3277
pos 3277 3277
stop_run
```


