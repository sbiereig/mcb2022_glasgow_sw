from amaranth import *

class ShiftRegInterface(Elaboratable):
    def __init__(self, sclk, dout, shld):
        self.sclk = sclk
        self.dout = dout
        self.shld = shld
        self.trigger = Signal(1)
        self.pos_ready = Signal(1)
        self.pos_x = Signal(12)
        self.pos_y = Signal(12)

    def elaborate(self, platform):
        m = Module()

        m.d.comb += [
            self.sclk.oe.eq(1),
            self.dout.oe.eq(0),
            self.shld.oe.eq(1),
        ]

        DIV_CYC = 32  # produce 1.5 MHz output clock for shift register
        tick = Signal(1)
        div = Signal(range(DIV_CYC))
        with m.If(div != 0):
            m.d.sync += div.eq(div - 1)
            m.d.sync += tick.eq(0)
        with m.Else():
            m.d.sync += div.eq(DIV_CYC-1)
            m.d.sync += tick.eq(1)

        # tick occurs on falling edge of SCLK
        # this is (where the state machine operates, giving around 300ns setup & hold for the SHLD/DOUT pins)
        m.d.sync += self.sclk.o.eq(~div[-1])

        # output shift format:
        # U5-H .. U5-A, U4-H .. U4-A, U3-H .. U3-A
        # bit order on serial line (=order in FPGA mirror shift register)
        # Y04, Y01, Y02, Y00, Y03, Y06, Y05, Y08, Y11, Y09, Y10, Y07, 
        # X00, X01, X02, X03, X07, X06, X05, X04, X08, X09, X10, X11
        shift_count = Signal(range(24))
        sr = Signal(24)
        m.d.comb += [
            #                 0       1       2       3       4       5       6       7       8       9       10      11 
            self.pos_x.eq(Cat(sr[12], sr[13], sr[14], sr[15], sr[19], sr[18], sr[17], sr[16], sr[20], sr[21], sr[22], sr[23])),
            #                 0       1       2       3       4       5       6       7       8       9       10      11 
            self.pos_y.eq(Cat(sr[3],  sr[1],  sr[2],  sr[4],  sr[0],  sr[6],  sr[5],  sr[11], sr[7],  sr[9],  sr[10], sr[8])),
        ]

        with m.FSM():
            with m.State("IDLE"):
                m.d.sync += self.pos_ready.eq(1)
                m.d.sync += self.shld.o.eq(0)  # keep shift register in "parallel load" state when idle
                m.d.sync += shift_count.eq(24)
                with m.If(self.trigger):
                    m.d.sync += self.pos_ready.eq(0)
                    m.next = "SAMPLE_BITS"
            with m.State("SAMPLE_BITS"):
                with m.If(tick):
                    m.d.sync += [
                        self.shld.o.eq(1),
                        sr.eq(Cat(sr[1:], self.dout.i)),
                        shift_count.eq(shift_count - 1),
                    ]
                with m.If(shift_count == 0):
                    m.next = "RESUME"
            with m.State("RESUME"):
                with m.If(tick):
                    m.d.sync += self.shld.o.eq(0)
                    m.next = "IDLE"
        
        return m

